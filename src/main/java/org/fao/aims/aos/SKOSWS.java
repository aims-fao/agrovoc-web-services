package org.fao.aims.aos;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.langsearch.SKOSSearchEngine;
import it.uniroma2.art.owlart.langsearch.SKOSSearchEngine.skosAllTypes;
import it.uniroma2.art.owlart.langsearch.analysis.ASCIIFoldingSingleCharLabelSeparatorTokenAnalyzer;
import it.uniroma2.art.owlart.langsearch.index.OntoTypeNotIndexableException;
import it.uniroma2.art.owlart.langsearch.index.RDFIndexManager;
import it.uniroma2.art.owlart.langsearch.index.config.StableAnalyzerConfiguration;
import it.uniroma2.art.owlart.langsearch.search.IndexAccessException;
import it.uniroma2.art.owlart.langsearch.search.OntIndexSearcher;
import it.uniroma2.art.owlart.langsearch.search.qcompilers.QueryCompiler;
import it.uniroma2.art.owlart.langsearch.search.qcompilers.SimpleUserQueryCompiler;
import it.uniroma2.art.owlart.langsearch.search.qcompilers.SimpleUserQueryCompiler.QueryModifier;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.navigation.RDFIterator;
import it.uniroma2.art.owlart.query.Binding;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.Update;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2PersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2RemoteModelConfiguration;
import it.uniroma2.art.owlart.utilities.ModelUtilities;
import it.uniroma2.art.owlart.utilities.PropertyChainsTree;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDFResourceRolesEnum;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SKOSWS {

	protected static Logger logger = LoggerFactory.getLogger(SKOSWS.class);
	protected String debugSeparator = "\n\t";
	
	private boolean useAutocommit;

	private boolean useResoner;
	
	private static boolean printQueryInDebug;

	private String repositoryDirPath;
	private String skosInputFilePath;
	private String indexDirPath;
	private String baseuri;

	private String fileNameProperty = "configWS.properties";
	private String fileNameVersion = "version.info";
	
	
	
	private String errorMessage = "There was a problem with the service ";

	final private String LUCENEINDEX = "http://www.ontotext.com/owlim/lucene#agrovoc";
	private static boolean useOWLIMIndexes;
	
	// private String skosInputFilePathWS = "..\\webapps\\resources\\agrovoc_wb_20110223_products_v_1_3.nt";
	// private String repositoryDirPathWS = "..\\webapps\\resources\\repDirSKOS";
	// private String indexDirWS = "..\\webapps\\resources\\indexDir";

	// private String skosInputFilePathLocal = "agrovoc_wb_20110223_products_v_1_3.nt";
	// private String skosInputFilePathLocal = "agrovoc_wb_20110223_v_1_3.nt";
	// private String repositoryDirPathLocal = "repDir";
	// private String indexDirLocal = "indexDir";

	private String lock = "lock";

	//private String baseUri = "http://aims.fao.org/aos/agrovoc";
	//private String importedUri = "http://aims.fao.org/aos/agrovoc";

	private static boolean initialized = false;

	private static SKOSXLModel skosxlModel = null;
	private static SKOSSearchEngine skosSearchEngine = null;

	private String WEBSERVICENAME = "vocWS";

	//private String NOTIMPLEMENTED = "Not yet implemented";
	private String DEPRECATED = "This service has been deprecated, and is no longer supported";

	//private String DEPRECATED = "deprecated";
	
	//private String BASEURI = "http://aims.fao.org/aos/agrovoc/";

	private String AGROVOC = "http://aims.fao.org/aos/agrovoc";
	
	private String LABEL="http://www.w3.org/2000/01/rdf-schema#label";
	
	private String PREFLABEL = "http://www.w3.org/2004/02/skos/core#prefLabel";
	private String ALTLABEL = "http://www.w3.org/2004/02/skos/core#altLabel";
	private String HIDDENLABEL = "http://www.w3.org/2004/02/skos/core#hiddenLabel";
	
	private String SEMANTICRELATION = "http://www.w3.org/2004/02/skos/core#semanticRelation";
	
	private String SKOSXLPREFLABEL = "http://www.w3.org/2008/05/skos-xl#prefLabel";
	private String SKOSXLALTLABEL = "http://www.w3.org/2008/05/skos-xl#altLabel";
	private String SKOSXLHIDDENLABEL = "http://www.w3.org/2008/05/skos-xl#hiddenLabel";
	private String SKOSXLLITERALFORM = "http://www.w3.org/2008/05/skos-xl#literalForm";
	
	private String BROADER = "http://www.w3.org/2004/02/skos/core#broader";
	private String CONCEPT = "http://www.w3.org/2004/02/skos/core#Concept";
	private String RELATED = "http://www.w3.org/2004/02/skos/core#related";
	private String NARROWER = "http://www.w3.org/2004/02/skos/core#narrower";
	
	private String BROADERTRANSIVE = "http://www.w3.org/2004/02/skos/core#broaderTransitive";
	private String NARROWERTRANSIVE = "http://www.w3.org/2004/02/skos/core#narrowerTransitive";
				
	private String TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
	private String LABEL_CLASS = "http://www.w3.org/2008/05/skos-xl#Label";
	
	private String DATATYPEPROPERTY = "http://www.w3.org/2002/07/owl#DatatypeProperty";
	private String OBJECTPROPERTY = "http://www.w3.org/2002/07/owl#ObjectProperty";
	
	//private String HASCOPENOTE= "http://aims.fao.org/aos/agrontology#hasScopeNote";
	private String HASCOPENOTE = "http://www.w3.org/2004/02/skos/core#scopeNote";
	private String ISPARTOFSSUBVOCABULARY = "http://aims.fao.org/aos/agrontology#isPartOfSubvocabulary"; 
	private String HASTERMTYPE = "http://aims.fao.org/aos/agrontology#hasTermType";
	private String HASEDITORIALNOTE = "http://aims.fao.org/aos/agrontology#hasEditorialNote";
	//private String HASDATEUPDATED = "http://aims.fao.org/aos/agrontology#hasDateLastUpdated";
	private String HASDATEUPDATED = "http://purl.org/dc/terms/modified";
	//private String HASDATECREATED = "http://aims.fao.org/aos/agrontology#hasDateCreated";
	private String HASDATECREATED = "http://purl.org/dc/terms/created";
	
	private String DEFINITION = "http://www.w3.org/2004/02/skos/core#definition";
	private String RDFVALUE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#value";
	
	private String DATETIME= "http://www.w3.org/2001/XMLSchema#dateTime";
	
	final static public String STRINGRDF = "http://www.w3.org/2001/XMLSchema#string";
	
	private String HASCODEFISCHERY3ALPHA = "http://aims.fao.org/aos/agrontology#hasCodeFishery3Alpha";
	private String HASCODETAXONOMY = "http://aims.fao.org/aos/agrontology#hasCodeTaxonomic";
	
	// It has been removed in the new version, now it is skos:notation with dataype 
	//private String HASCODEAGROVOC = "http://aims.fao.org/aos/agrontology#hasCodeAgrovoc";
	private String NOTATION = "http://www.w3.org/2004/02/skos/core#notation";
	private String AGROVOCCODE = "http://aims.fao.org/aos/agrovoc/AgrovocCode";
	
	
	private String HASCODEASFA = "http://aims.fao.org/aos/agrontology#hasCodeAsfa";
	private String HASCODEFAOPA = "http://aims.fao.org/aos/agrontology#hasCodeFaoPa";
	private String HASCODEFAOTERM = "http://aims.fao.org/aos/agrontology#hasCodeFaoterm";
	private String HASCODEASC ="http://aims.fao.org/aos/agrontology#hasCodeAsc";
	private String HASCODEISO3COUNTRY = "http://aims.fao.org/aos/agrontology#hasCodeISO3Country";
	

	private String[] languages = { "ar", "cs", "de", "en", "es", "fa", "fr", "hi", "hu", "id", "it", "ja",
			"ko", "lo", "mr", "ms", "pl", "pt", "ru", "sk", "te", "th", "tr", "uk", "zh" };

	public SKOSWS() {
		// repositoryDirPath = repositoryDirPathWS;
		// skosInputFilePath = skosInputFilePathWS;
		initialize();
	}

	/*
	 * public SKOSWS(boolean useLocal){ if(useLocal){ repositoryDirPath = repositoryDirPathLocal;
	 * skosInputFilePath = skosInputFilePathLocal; indexDir = indexDirLocal; } else{ repositoryDirPath =
	 * repositoryDirPathWS; skosInputFilePath = skosInputFilePathWS; indexDir = indexDirWS; } initialize(); }
	 */

	private void deleteLock() {
		File lockDir = new File(repositoryDirPath + "\\" + lock);
		if (lockDir.exists() || lockDir.isDirectory()) {
			File[] files = lockDir.listFiles();
			for (int i = 0; i < files.length; i++) {
				files[i].delete();
			}
			lockDir.delete();
		}
	}

	private void initialize() {
		long start = System.currentTimeMillis();
		long time;
		
		//check if the WS has been already initialized
		if(initialized)
			return;
		initialized = true;
		
		try {
			String filePropertyFilePath = findPropertyFile();
			logger.info("Property file: "+filePropertyFilePath.trim());

			Properties configFile = new Properties();
			InputStream is = new FileInputStream(filePropertyFilePath);
			configFile.load(is);

			repositoryDirPath = configFile.getProperty("repositoryDirPath");
			skosInputFilePath = configFile.getProperty("skosInputFilePath");
			indexDirPath = configFile.getProperty("indexDirWS");
			baseuri = configFile.getProperty("baseURI");
			
			useOWLIMIndexes = Boolean.parseBoolean(configFile.getProperty("owlim.useIndexes"));
			boolean reloadOWLIMIndexes = Boolean.parseBoolean(configFile.getProperty("owlim.reloadIndexes"));
			
			printQueryInDebug = Boolean.parseBoolean(configFile.getProperty("printQueryInDebug"));
			boolean forceReload = Boolean.parseBoolean(configFile.getProperty("forceReload"));
			
			//if the last time the WS was closed not in the correct way the lock may be this present,
			// delete it
			deleteLock();

			
			
			//check in the property file if the user want to use OWLIM or SESAME2
			String useRemoteOrLocal = configFile.getProperty("useREMOTEorLOCAL");
			logger.info("Using "+useRemoteOrLocal.trim()+" repository");
			//System.out.println("Using "+useRemoteOrLocal.trim()+" repository");
			ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();
			if(useRemoteOrLocal.trim().compareToIgnoreCase("remote") == 0){
				Sesame2RemoteModelConfiguration sesame2RemoteModelCong = 
						factImpl.createModelConfigurationObject(Sesame2RemoteModelConfiguration.class);
				sesame2RemoteModelCong.username = configFile.getProperty("remote.username");
				sesame2RemoteModelCong.password = configFile.getProperty("remote.password");
				sesame2RemoteModelCong.serverURL = configFile.getProperty("remote.url");
				sesame2RemoteModelCong.repositoryId = configFile.getProperty("remote.repositoryId");
				
				// check if the directory for the repository exists, otherwise create it
				File repDirFile = new File(repositoryDirPath);
				logger.info("repositoryDirPath =  "+ repDirFile.getAbsolutePath());
				if (!repDirFile.exists()) {
					repDirFile.mkdirs();
				} /*else if (repDirFile.list().length == 0) {
					loadRep = true;
				} else if(forceReload){// forceReload is set, in this case delete and create the directory
					delete(repDirFile);
					repDirFile.mkdirs();
				}*/
				
				logger.info(System.currentTimeMillis()-start +" millisec to prepare to load the SKOSXL model");
				//System.out.println(System.currentTimeMillis()-start +" millisec to prepare to load the SKOSXL model"); // DEBUG
				skosxlModel = factImpl.loadSKOSXLModel(baseuri, repositoryDirPath, sesame2RemoteModelCong);
				logger.info(System.currentTimeMillis()-start +" millisec to load the SKOSXL model");
				//System.out.println(System.currentTimeMillis()-start +" millisec to load the SKOSXL model"); // DEBUG
				time = System.currentTimeMillis();
				
				/*if (loadRep || forceReload) {
					System.out.println("reload SKOS model loadRep = " + loadRep + 
							" forceReload = " + forceReload);
					skosxlModel.close();
					skosxlModel = factImpl.loadSKOSXLModel(baseUri, repositoryDirPath, sesame2RemoteModelCong);
				}*/
				skosxlModel.setBaseURI(baseuri);
				
				//check if the config file decided to use the OWLIM indexes and it wants to create/reload them
				logger.info("useOWLIMIndexes = "+useOWLIMIndexes);
				logger.info("reloadOWLIMIndexes "+reloadOWLIMIndexes);
				if(useOWLIMIndexes && reloadOWLIMIndexes){
					createOWLIMIndexes();
					logger.info("OWLIM Indexes created in "+(System.currentTimeMillis()-start)+" millisec");
					time = System.currentTimeMillis();
					
				}
				
			} else{ // using the local repository with sesame2
				OWLArtModelFactory<Sesame2ModelConfiguration> fact = OWLArtModelFactory
						.createModelFactory(factImpl);
				Sesame2PersistentInMemoryModelConfiguration modelConf;
				modelConf = factImpl
						.createModelConfigurationObject(Sesame2PersistentInMemoryModelConfiguration.class);
				// modelConf.rdfsInference = false;
				// modelConf.directTypeInference = false;

				// check if the directory for the repository exists, otherwise create it
				File repDirFile = new File(repositoryDirPath);
				boolean loadRep = false;
				if (!repDirFile.exists()) {
					repDirFile.mkdirs();
					loadRep = true;
				} else if (repDirFile.list().length == 0) {
					loadRep = true;
				} else if(forceReload){// forceReload is set, in this case delete and create the directory
					delete(repDirFile);
					repDirFile.mkdirs();
				}

				logger.info("loading the SKOSXL model");
				//System.out.println("loading the SKOSXL model"); // DEBUG
				skosxlModel = fact.loadSKOSXLModel(baseuri, repositoryDirPath, modelConf);
				
				logger.info(System.currentTimeMillis()-start +" millisec to load the SKOSXL model");
				//System.out.println(System.currentTimeMillis()-start +" millisec to load the SKOSXL model"); // DEBUG
				time = System.currentTimeMillis();
				
				// skosxlModel.addRDF(new File(skosInputFilePath), importedUri, RDFFormat.NTRIPLES,
				// NodeFilters.MAINGRAPH);
				if (loadRep || forceReload) {
					logger.info("reload SKOS model loadRep = " + loadRep + 
							" forceReload = " + forceReload);
					//System.out.println("reload SKOS model loadRep = " + loadRep + 
					//		" forceReload = " + forceReload);
					
					File rdfFileToAdd = new File(skosInputFilePath);
					RDFFormat rdfFormat = RDFFormat.guessRDFFormatFromFile(rdfFileToAdd);
					
					//skosxlModel.addRDF(rdfFileToAdd, null, RDFFormat.NTRIPLES, NodeFilters.MAINGRAPH); // old
					skosxlModel.addRDF(rdfFileToAdd, null, rdfFormat, NodeFilters.MAINGRAPH);
					
					
					logger.info(System.currentTimeMillis()-time +" millisec to add the SKOSXL file");
					//System.out.println(System.currentTimeMillis()-time +" millisec to add the SKOSXL file"); // DEBUG
					time = System.currentTimeMillis();
					
					logger.info("saving the model");
					//System.out.println("saving the model");
					skosxlModel.close();
					logger.info("model saved in "+(System.currentTimeMillis()-time)+" millisec");
					//System.out.println("model saved in "+(System.currentTimeMillis()-time)+" millisec");
					time = System.currentTimeMillis();
					logger.info("reloading the model");
					//System.out.println("reloading the model");
					skosxlModel = fact.loadSKOSXLModel(baseuri, repositoryDirPath, modelConf);
					logger.info("model reloaded in "+(System.currentTimeMillis()-time)+" millisec");
					//System.out.println("model reloaded in "+(System.currentTimeMillis()-time)+" millisec");
					
				}
				skosxlModel.setBaseURI(baseuri);
			}
			

			// Initialize the Search Engine
			// check if the directory for the repository exists, otherwise create it (only if the config file)
			// did not selected the OWLIM indexes
			if(useRemoteOrLocal.trim().compareToIgnoreCase("remote") != 0 || !useOWLIMIndexes){
				File indexDirFile = new File(indexDirPath);
				boolean createIndexes = false;
				if (!indexDirFile.exists()) {
					indexDirFile.mkdirs();
					createIndexes = true;
				} else if (indexDirFile.list().length == 0) {
					createIndexes = true;
				} else if(forceReload){// forceReload is set, in this case delete and create the directory
					delete(indexDirFile);
					indexDirFile.mkdirs();
					createIndexes = true;
				}
				StableAnalyzerConfiguration conf = new StableAnalyzerConfiguration(
						new ASCIIFoldingSingleCharLabelSeparatorTokenAnalyzer(Version.LUCENE_36), languages);
				//skosSearchEngine = new SKOSSearchEngine(skosxlModel, indexDirPath, languages);
				
				skosSearchEngine = new SKOSSearchEngine(skosxlModel, indexDirPath, conf); 
				if (createIndexes || forceReload) {
					logger.info("reload indexes createIndexes = " + createIndexes + " forceReload = "
							+ forceReload);
					//System.out.println("reload indexes createIndexes = " + createIndexes + " forceReload = "
					//		+ forceReload);
					ARTURIResource conceptScheme = skosxlModel
							.createURIResource("http://aims.fao.org/aos/agrovoc");
					
					
					//skosSearchEngine.buildConceptIndex(true, conceptScheme);
					//skosSearchEngine.buildIndex(true);
					ARTURIResourceIterator it = skosxlModel.listConceptsInScheme(conceptScheme);
					skosSearchEngine.setInference(true);
					skosSearchEngine.buildCustomIndex(RDFResourceRolesEnum.concept.toString(), 
							it, RDFS.Res.LABEL, true);
							//it, SKOS.Res.PREFLABEL, true);
					
					skosSearchEngine.initializeStandardIndexWriters();
	
					skosSearchEngine.setTHRESHOLD(0.9); // check if this threshold is correct or if it should be
					// changed
				}	
	
				skosSearchEngine.createOntIndexSearcher();
				logger.info(System.currentTimeMillis()-time +" millisec to load the Indexes file");
				//System.out.println(System.currentTimeMillis()-time +" millisec to load the Indexes file"); // DEBUG
			}
			
			//close the open streams
			is.close();
			
			

		} catch (UnsupportedModelConfigurationException e) {
			e.printStackTrace();
		} catch (UnloadableModelConfigurationException e) {
			e.printStackTrace();
		} catch (ModelCreationException e) {
			e.printStackTrace();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
		} 
	}

	 private static void delete(File file) throws IOException{
    	if(file.isDirectory()){
    		//directory is empty, then delete it
    		if(file.list().length==0){
    		   file.delete();
    		}else{
    		   //list all the directory contents
        	   String files[] = file.list();
        	   for (String temp : files) {
        	      //construct the file structure
        	      File fileDelete = new File(file, temp);
        	      //recursive delete
        	     delete(fileDelete);
        	   }
        	   //check the directory again, if empty then delete it
        	   if(file.list().length==0){
           	     file.delete();
        	   }
    		}
    	}else{
    		//if file, then delete it
    		file.delete();
    	}
    }
	
	private String findPropertyFile() {
		logger.info("Root directory = "+(new File(".")).getAbsolutePath());
		 File file = new File(fileNameProperty); 
		 // useful only if launched and the property file is in the same directory
         // for example when launched inside Eclipse
         if (!file.exists()) { // search the property file recursive in all the directory
                File propFile = searchFile("..", fileNameProperty);
                if(propFile!=null) {
                      return propFile.getAbsolutePath();
                }
         } else
                return fileNameProperty;
         // no property file was found
         return "";
	}
	
	/*
     * Fabrizio Celli added 04/02/2013
     * Recursively search a file in a directory given the filename
     * @param rootS the root directory to start the search
     * @param fileName the name of the file to search
     * @return the found File, null if the file is not in
     */
     private File searchFile(String rootS, String fileName){
    	 File root = new File(rootS);
    	 if(root!=null && root.exists() && root.isDirectory() && root.canRead()){
    		 try {
    			 boolean recursive = true;
    			 Collection<?> files = FileUtils.listFiles(root, null, recursive);
    			 for (Iterator<?> iterator = files.iterator(); iterator.hasNext();) {
    				 File file = (File) iterator.next();
    				 if (!file.isDirectory() && file.getName().equalsIgnoreCase(fileName))
    					 return file;
    			 	}
    		 } catch (Exception e) {
    			 e.printStackTrace();
    		 }
    	 }
    	 return null;
     }

     
    private void createOWLIMIndexes(){
    	String query = "";
		
		try {
			
			query = "PREFIX luc: <http://www.ontotext.com/owlim/lucene#>" +
					"\nINSERT DATA {" +
					"\nluc:moleculeSize luc:setParam \"1\" ."+
					"\nluc:languages luc:setParam \"\" . "+
					"\nluc:include luc:setParam \"centre\" . "+
					"\nluc:index luc:setParam \"literals\" . "+
					"\n}";
			//execute this query
			Update insertDataQuery = skosxlModel.createUpdateQuery(QueryLanguage.SPARQL, query);
			insertDataQuery.evaluate(true);
			
			// prepare a query to create the index 
			// the name of the index is luc:vocbench
			query = "PREFIX luc: <http://www.ontotext.com/owlim/lucene#>" + 
					"\nINSERT DATA { " + 
					"\n<"+LUCENEINDEX+"> luc:createIndex \"true\" . " + 
					"\n}";
			//execute this query
			Update insertDataQuery2 = skosxlModel.createUpdateQuery(QueryLanguage.SPARQL, query);
			insertDataQuery2.evaluate(true);
			
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
    } 
     
	public String getAgrovocCSModules() {
		return DEPRECATED;
	}

	public String getAgrovocCSLanguages() {
		return DEPRECATED;
	}

	public String getAgrovocCSLanguagesExpand() {
		return DEPRECATED;
	}

	public String getAvailableCode(String ontologyName) {
		return DEPRECATED;
	}

	public String getCodesByURI(String Ontology, String ConceptURI, String CodeName) {
		return DEPRECATED;
	}

	//TODO DONE
	public String getConceptInfoByClassName(String ontologyName, String conceptName, String format) {
//		logger.debug("getConceptInfoByClassName:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"conceptName="+conceptName+
//				debugSeparator+"format="+format);
		String result = "";
		// initialize();

		if(conceptName.startsWith("agrovoc:"))
			conceptName = conceptName.replace("agrovoc:", "");
		
		try {
			if (format.compareToIgnoreCase("skos") == 0){
				// Create a SPARQL query with all the retrieve result
				String query = "";
				query = "CONSTRUCT { ?conceptURI ?pred1 ?label . " +
						"\n ?conceptURI ?pred2 ?obj2 ." +
						"\n ?obj2 ?pred3 ?label3 }"+
						"\nWHERE { "+
				
						"\nFILTER( ?conceptURI = <"+skosxlModel.getBaseURI() + conceptName+"> )"+
				
						"\n{ "+
						"\n ?conceptURI ?pred1 _:b1 . "+
						//"\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
						//"\n?pred1 = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label . "+
						"\n} "+
						"\nUNION { "+
						"\n ?conceptURI ?pred2 ?obj2 . "+
						"\nFILTER(?pred2 != <"+SKOSXLALTLABEL+"> && "+
						"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
						"\n?pred2 != <"+SKOSXLHIDDENLABEL+"> && "+
						"\n?pred2 != <"+LABEL+"> && "+
						"\n?pred2 != <"+PREFLABEL+"> && "+
						"\n?pred2 != <"+ALTLABEL+"> && "+
						"\n?pred2 != <"+HIDDENLABEL+"> && "+
						"\n?pred2 != <"+SEMANTICRELATION+">) . "+
						"\nOPTIONAL{ "+
						"\n	?obj2 a <"+CONCEPT+"> . "+
						"\n	?obj2 ?pred3 _:b3 . "+
						//"\n	FILTER(?pred3 = <"+SKOSXLALTLABEL+"> || "+
						//"\n?pred3 = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n	_:b3 <"+SKOSXLLITERALFORM+"> ?label3 . "+
						"\n} "+
						"\n} "+
						"\n}";
				printQuery(query);
				
				GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
				ARTStatementIterator it = graphQuery.evaluate(true);
				
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
				skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
				result = out.toString();
				it.close();
				out.close();
				writer.close();
				
			}else if (format.compareToIgnoreCase("txt") == 0){
				// Create a SPARQL query with all the retrieve result
				String query = "SELECT ?conceptURI ?label "+ 
						"\nWHERE { "+
						"\n ?conceptURI ?pred1 _:b1 . "+

						"\nFILTER( ?conceptURI = <"+skosxlModel.getBaseURI() + conceptName+"> )"+

						"\n FILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
						"?pred1 = <"+SKOSXLHIDDENLABEL+"> || "+
						"?pred1 = <"+SKOSXLPREFLABEL+"> ) ."+
						"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label ."+
						"}";
				printQuery(query);
				
				TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
				TupleBindingsIterator it = tupleQuery.evaluate(true);
				result = "[";
				String lastConceptURI = "";
				while(it.hasNext()){
					TupleBindings tuple = it.next();
					Iterator<Binding> iterInTuple = tuple.iterator();
					String conceptId = iterInTuple.next().getBoundValue().asURIResource().getLocalName();
					if(lastConceptURI.compareToIgnoreCase(conceptId) != 0) {// new concept
						lastConceptURI = conceptId;
						if(result.length()>1){
							result += "][";
						}
					}
					else if(result.length()>1)
						result +=",";
					ARTLiteral literal = iterInTuple.next().getBoundValue().asLiteral();
					result += literal.getLabel()+" ("+literal.getLanguage()+")";
				}
				result +="]";
				it.close();
			}
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByClassName"; 
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByClassName"; 
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByClassName"; 
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByClassName"; 
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByClassName"; 
		} catch (IOException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByClassName"; 
		}
		return result;
	}

	// TODO DONE
	/**
	 * The Method will return the concept information for the given URI. The result of this method will be 
	 * in SKOS or TXT format.
	 * @param ontologyName
	 * @param conceptURI
	 * @param format SKOS or TXT
	 * @return the concept information for the given URI
	 */
	public String getConceptInfoByURI(String ontologyName, String conceptURI, String format) {
//		logger.debug("getConceptInfoByURI:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"conceptURI="+conceptURI+
//				debugSeparator+"format="+format);
		String result = "";

		//In this method, it is returning all the properties and their values for given concept and if the 
		// value is Concept, then also include all the labels.
		
		
		
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
			if(format.compareToIgnoreCase("skos") == 0) {
				//Prepare a complex query which construct a graph containing all the result of this method
				String query = "CONSTRUCT {" +
						"\n<"+conceptURI+"> ?pred1 ?label1 . " +
						"\n<"+conceptURI+"> ?pred2 ?obj2 ." +
						"\n<"+conceptURI+"> ?pred3 ?obj3 ." +
						"\n ?obj3 ?pred31 ?label3 ." +
						"\n<"+conceptURI+"> <"+NARROWER+"> ?subjBroader ." +
						"\n?subjBroader ?pred41 ?label4 }" +
						
						"\nWHERE { "+
						"\n{ "+
						"\n<"+conceptURI+"> ?pred1 _:b1 . "+
						"\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
						"\n?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"\n?pred1 = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label1 . "+
						"\n} "+
						
						"\nUNION { "+
						"\n<"+conceptURI+"> ?pred2 ?obj2 . "+
						"\nFILTER(?pred2 = <"+HASCOPENOTE+"> || "+
						"\n?pred2 = <"+HASEDITORIALNOTE+"> || "+
						"\n?pred2 = <"+HASDATEUPDATED+">) . "+
						"\n}"+
						
						"\nUNION { "+
						"\n<"+conceptURI+"> ?pred3 ?obj3 . "+
						"\n?obj3 a <"+CONCEPT+"> . "+
						"\nFILTER(?pred3 = <"+BROADER+"> || "+
						"\n?pred3 = <"+RELATED+"> ). "+
						"\n?obj3 ?pred31 _:b3"+
						"\n	FILTER(?pred31 = <"+SKOSXLALTLABEL+"> || "+
						"\n?pred31 = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"\n?pred31 = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n	_:b3 <"+SKOSXLLITERALFORM+"> ?label3 . "+
						"\n} "+
						
						//Since NARROWER is not normally used, it is better to use 
						// ?c2 BROADER ?c1
						//which is equivalent (but present) to
						// ?c1 NARROWER ?c2
						// so the results will have (left side in the WHERE and right side in the CONSCTRUCT)
						// ?c2 BROADER ?c1  ->  ?c1 NARROWER ?c2
						// ?c1 BROADER ?c3  ->  ?c1 BROADER ?c3
						"\nUNION { "+
						"\n?subjBroader <"+BROADER+"> <"+conceptURI+"> . "+
						"\n?subjBroader a <"+CONCEPT+"> . "+
						"\n?subjBroader ?pred41 _:b4"+
						"\n	FILTER(?pred41 = <"+SKOSXLALTLABEL+"> || "+
						"\n?pred41 = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"\n?pred41 = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n	_:b4 <"+SKOSXLLITERALFORM+"> ?label4 . "+
						"\n} "+
						
						"\n}";
				GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
				ARTStatementIterator it = graphQuery.evaluate(true);
				
				skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
				it.close();
				out.close();
				writer.close();
				result = out.toString();
			} else{ // format == "txt"
				//prepare a query which extract only the label of the selected concept
				String query = "SELECT ?label "+ 
						"\nWHERE { "+
						"\n<"+conceptURI+"> ?pred1 _:b1 . "+
						"\n FILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
						"?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"?pred1 = <"+SKOSXLPREFLABEL+"> ) ."+
						"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label ."+
						"}";
				TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
				TupleBindingsIterator it = tupleQuery.evaluate(true);
				result = "[";
				while(it.hasNext()){
					if(result.length()>1)
						result +=",";
					TupleBindings tuple = it.next();
					ARTLiteral literal = tuple.iterator().next().getBoundValue().asLiteral();
					result += literal.getLabel()+" ("+literal.getLanguage()+")";
				}
				it.close();
				result +="]";
			}

		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByURI";
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByURI";
		} catch (IOException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByURI";
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByURI";
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByURI";
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getConceptInfoByURI";
		}

		return result;
	}

	//TODO DONE
	public String getDataTypeRelationship(String ontologyName) {
//		logger.debug("getDataTypeRelationship:"+
//				debugSeparator+"ontologyName="+ontologyName);
		String result = "";
		List<String> objPropList = new ArrayList<String>();
		try {
			ARTURIResource type = skosxlModel.createURIResource(TYPE);
			ARTURIResource datatypeProperty = skosxlModel.createURIResource(DATATYPEPROPERTY);
			ARTURIResource label = skosxlModel.createURIResource(LABEL);

			ARTStatementIterator iter = skosxlModel.listStatements(NodeFilters.ANY, type, datatypeProperty,
					false, NodeFilters.ANY);

			int cont = 1;
			while (iter.hasNext()) {
				ARTURIResource datatypeProp = iter.next().getSubject().asURIResource();
				String objPropURI = datatypeProp.getURI();
				String objPropName = datatypeProp.getLocalName();
				if (!objPropList.contains(objPropURI)) {
					objPropList.add(objPropURI);
					result += cont + " URI " + objPropURI + "\n";
					result += cont + " NAME " + objPropName + "\n";
					// TODO add the labels
					ARTStatementIterator iter2 = skosxlModel.listStatements(datatypeProp, label,
							NodeFilters.ANY, false, NodeFilters.ANY);
					int cont2 = 1;
					while (iter2.hasNext()) {
						ARTStatement artStatement = iter2.next();
						ARTNode artNode = artStatement.getObject();
						if (artNode.isLiteral()) {
							ARTLiteral artLiteral = artNode.asLiteral();
							result += cont + "." + cont2 + " Label " + artLiteral.getLabel() + " Language "
									+ artLiteral.getLanguage() + "\n";
						}
					}
					iter2.close();
					++cont;
				}
			}
			iter.close();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		}

		return result;
		// return NOTIMPLEMENTED;
	}

	public String getDefinitionsByClassName(String ontologyName, String className, String format,
			String outputlang) {
		return DEPRECATED;
	}

	public String getDefinitionsByURI(String ontologyName, String conceptURI, String format, String outputlang) {
		return DEPRECATED;
	}

	public String getLanguageCode(String localLang) {
		return DEPRECATED;
	}

	//TODO DONE
	public String getObjectPropertyRelationship(String ontologyName) {
//		logger.debug("getObjectPropertyRelationship:"+
//				debugSeparator+"ontologyName="+ontologyName);
		String result = "";
		List<String> objPropList = new ArrayList<String>();
		try {
			ARTURIResource type = skosxlModel.createURIResource(TYPE);
			ARTURIResource objectProperty = skosxlModel.createURIResource(OBJECTPROPERTY);
			ARTURIResource label = skosxlModel.createURIResource(LABEL);

			ARTStatementIterator iter = skosxlModel.listStatements(NodeFilters.ANY, type, objectProperty,
					false, NodeFilters.ANY);

			int cont = 1;
			while (iter.hasNext()) {
				ARTURIResource objProp = iter.next().getSubject().asURIResource();
				String objPropURI = objProp.getURI();
				String objPropName = objProp.getLocalName();
				if (!objPropList.contains(objPropURI)) {
					objPropList.add(objPropURI);
					result += cont + ". URI " + objPropURI + "\n";
					result += cont + ". NAME " + objPropName + "\n";
					// TODO add the labels
					ARTStatementIterator iter2 = skosxlModel.listStatements(objProp, label, NodeFilters.ANY,
							false, NodeFilters.ANY);
					int cont2 = 1;
					while (iter2.hasNext()) {
						ARTStatement artStatement = iter2.next();
						ARTNode artNode = artStatement.getObject();
						if (artNode.isLiteral()) {
							ARTLiteral artLiteral = artNode.asLiteral();
							result += cont + "." + cont2 + " Label " + artLiteral.getLabel() + " Language "
									+ artLiteral.getLanguage() + "\n";
						}
					}
					++cont;
				}
			}
			iter.close();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		}

		return result;
		// return NOTIMPLEMENTED;
	}

	//TODO DONE
	public String getPossibleRelationsFromConceptURI(String ontologyName, String conceptURI,
			String relationType) {
//		logger.debug("getPossibleRelationsFromConceptURI:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"conceptURI="+conceptURI+
//				debugSeparator+"relationType="+relationType);
		ARTURIResource concept = skosxlModel.createURIResource(conceptURI);
		List<String> uriList = new ArrayList<String>();
		int relationTypeInt = -1; // all = 0, objecttype = 1, datatype = 2
		String relationTypeString = relationType.toLowerCase();
		if (relationTypeString.compareTo("all") == 0)
			relationTypeInt = 0;
		else if (relationTypeString.compareTo("objecttype") == 0)
			relationTypeInt = 1;
		else if (relationTypeString.compareTo("datatype") == 0)
			relationTypeInt = 2;
		String result = "";
		try {
			// RDFIterator<ARTStatement> iter = new ConceptInfoRDFIterator(concept);
			ARTStatementIterator iter = skosxlModel.listStatements(concept, NodeFilters.ANY, NodeFilters.ANY,
					false, NodeFilters.ANY);
			boolean first = true;
			while (iter.hasNext()) {
				ARTStatement artStat = iter.next();
				ARTURIResource artUriRes = artStat.getPredicate();
				ARTNode artNodeObj = artStat.getObject();
				boolean goAlong = false;
				if (relationTypeInt == 0)
					goAlong = true;
				// else if((artNodeObj.isBlank() || artNodeObj.isURIResource()) && relationTypeInt == 1)
				else if (artNodeObj.isResource() && relationTypeInt == 1)
					goAlong = true;
				else if (artNodeObj.isLiteral() && relationTypeInt == 2)
					goAlong = true;

				if (goAlong == false)
					continue;

				String localName = artUriRes.getLocalName();
				String uri = artUriRes.getURI();
				if (!uriList.contains(uri)) {
					uriList.add(uri);
					if(first)
						first = false;
					else
						result += "\n";
					result += "URI: " + uri;
					result += "Name: " + localName;
				}
			}
			iter.close();

		} catch (ModelAccessException e) {
			e.printStackTrace();
		}

		return result;
	}

	//TODO DONE
	public String getRelatedValuesFromConceptURIRelationURI(String ontologyName, String conceptURI,
			String relationURI) {
//		logger.debug("getRelatedValuesFromConceptURIRelationURI:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"conceptURI="+conceptURI+
//				debugSeparator+"relationURI="+relationURI);
		ARTURIResource concept = skosxlModel.createURIResource(conceptURI);
		ARTURIResource predicate = skosxlModel.createURIResource(relationURI);
		String result = "";
		try {
			ARTStatementIterator iter = skosxlModel.listStatements(concept, predicate, NodeFilters.ANY,
					false, NodeFilters.ANY);
			boolean first = true;
			while (iter.hasNext()) {
				ARTStatement artStat = iter.next();
				if(first)
					first = false;
				else
					result += "\n";
				
				if (artStat.getObject().isURIResource())
					result += artStat.getObject().asURIResource().getURI();
				else if (artStat.getObject().isLiteral())
					result += artStat.getObject().asLiteral().getLabel();
			}
			iter.close();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String getStatusConcept(String ontologyName, String conceptURI, String lang) {
		return DEPRECATED;
	}

	public String getStatusList(String ontologyName) {
		return DEPRECATED;
	}

	public String getStatusTerm(String ontologyName, String termCode, String codeName, String lang) {
		return DEPRECATED;
	}

	//TODO DONE
	/**
	 * The Method will return the list of terms code in the given term and language. The result of this 
	 * method will be in TXT or SKOS format.
	 * @param ontologyName
	 * @param term
	 * @param lang
	 * @param codeName
	 * @param format TXT or SKOS
	 * @return list of terms code in the given term and language
	 */
	public String getTermCodeByTermAndLangXML(String ontologyName, String term, String lang, String codeName,
			String format) {
//		logger.debug("getTermCodeByTermAndLangXML:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"term="+term+
//				debugSeparator+"lang="+lang+
//				debugSeparator+"codeName="+codeName+
//				debugSeparator+"format="+format);
		String result ="";
		// the code name should be the local name of one of the following properties:
		//		http://aims.fao.org/aos/agrontology#hasCodeFishery3Alpha
		//		http://aims.fao.org/aos/agrontology#hasCodeTaxonomic
		//		http://aims.fao.org/aos/agrontology#hasCodeAgrovoc
		//		http://aims.fao.org/aos/agrontology#hasCodeAsfa
		//		http://aims.fao.org/aos/agrontology#hasCodeFaoPa
		//		http://aims.fao.org/aos/agrontology#hasCodeFaoterm
		//		http://aims.fao.org/aos/agrontology#hasCodeAsc
		//		http://aims.fao.org/aos/agrontology#hasCodeISO3Country

		Map <String, String>propertyMap = new HashMap<String, String>();
		propertyMap.put("hasCodeFishery3Alpha", HASCODEFISCHERY3ALPHA);
		propertyMap.put("hasCodeTaxonomic", HASCODETAXONOMY);
		//propertyMap.put("hasCodeAgrovoc", HASCODEAGROVOC);// old
		propertyMap.put("notation", NOTATION);
		propertyMap.put("hasCodeAsfa", HASCODEASFA);
		propertyMap.put("hasCodeFaoPa", HASCODEFAOPA);
		propertyMap.put("hasCodeFaoterm", HASCODEFAOTERM);
		propertyMap.put("hasCodeAsc", HASCODEASC);
		propertyMap.put("hasCodeISO3Country", HASCODEISO3COUNTRY);
		
		String desiredProperty = propertyMap.get(codeName);
		if(desiredProperty == null){
			result = "The codeName should have one of the following values: ";
			for(String key : propertyMap.keySet())
				result += key+", "; 
			result = result.substring(0, result.length()-2);
			return result;
		}
		
		try {
			Collection<ARTURIResource> retrievedResources = null;
			retrievedResources = searchConcept(term, "exact match", lang);
			
//			logger.debug("getTermCodeByTermAndLangXML: number of results from search = "+retrievedResources.size());
			result = "";
			if(format.compareToIgnoreCase("txt") == 0){
				
				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					String query;
					query = "SELECT DISTINCT ?code" +
							"\nWHERE {" +
							"\n?concept ?pred1 _:b1.";
					query += "\nFILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?concept = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query +="\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || " +
							"?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"?pred1 = <"+SKOSXLPREFLABEL+"> )" +
							//"\nFILTER(?pred1 = <"+SKOSXLPREFLABEL+"> )" +
							"\n_:b1 <" +desiredProperty+"> ?code ."+ 
							"\n}";
					printQuery(query);
					TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
					TupleBindingsIterator it = tupleQuery.evaluate(true);

					
					while(it.hasNext()){
						TupleBindings tuple = it.next();
						Iterator<Binding> iterInTuple = tuple.iterator();
						ARTLiteral label = iterInTuple.next().getBoundValue().asLiteral();
						result += label.getLabel()+",";
					}
					it.close();
					if(result.length()>1){
						result = result.substring(0, result.length()-1);
					}
				}
			}else if(format.compareToIgnoreCase("skos") == 0) {
				
				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
					String query;
					query = "CONSTRUCT {" +
							"?concept <"+desiredProperty +"> ?code ."+
							"}" +
							"\nWHERE {" +
							"\n?concept ?pred1 _:b1.";
					query += "\nFILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?concept = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query +="\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || " +
							"?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"?pred1 = <"+SKOSXLPREFLABEL+"> )" +
							//"\nFILTER(?pred1 = <"+SKOSXLPREFLABEL+"> )" +
							"\n_:b1 <" +desiredProperty+"> ?code ."+ 
							"\n}";
					printQuery(query);				
					GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
					ARTStatementIterator it = graphQuery.evaluate(true);
					
					skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
					result = out.toString();
					it.close();
					out.close();
					writer.close();
				}
			}else
				return "The format must be txt or skos";
		}catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getTermCodeByTermAndLangXML";
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
			return errorMessage+"getTermCodeByTermAndLangXML"; 
		} catch (IOException e) {
			e.printStackTrace();
			return errorMessage+"getTermCodeByTermAndLangXML"; 
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getTermCodeByTermAndLangXML"; 
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getTermCodeByTermAndLangXML"; 
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getTermCodeByTermAndLangXML"; 
		} catch (IndexAccessException e) {
			e.printStackTrace();
			return errorMessage+"getTermCodeByTermAndLangXML"; 
		} catch (ParseException e) {
			e.printStackTrace();
			return errorMessage+"getTermCodeByTermAndLangXML"; 
		}
		
		return result;
	}

	
	// TODO DONE
	/**
	 * The Method will return the term expansion in the given search term, and can filter with the different 
	 * search mode and language. The result of this method will be in TXT or SKOS format
	 * (at the moment only the SKOS format is implemented).
	 * @param ontologyName
	 * @param searchString
	 * @param format
	 * @param searchMode (contains, exact match, starts with, ends with, exact word)
	 * @param lang
	 * @return
	 */
	public String getTermExpansion(String ontologyName, String searchString, String format,
			String searchMode, String lang) {
//		logger.debug("getTermExpansion:"+
//			debugSeparator+"ontologyName="+ontologyName+
//			debugSeparator+"searchString="+searchString+
//			debugSeparator+"format="+format+
//			debugSeparator+"searchMode="+searchMode+
//			debugSeparator+"lang="+lang);
		
		Collection<ARTLiteral> retrievedResources = null;
		String result = "";
		
		boolean filterOutputWithLang = false;
		
		if(ontologyName.compareToIgnoreCase("agrovoc") != 0)
			return DEPRECATED+" for ontologyName = "+ontologyName;
		
		try {
			
			retrievedResources = searchLabelsCaseInsensitive(searchString, searchMode, lang);
			
			if(retrievedResources == null)
				return DEPRECATED+" search mode: "+searchMode;
			// return the result depending on the requested format
			// check the output language, the FAO Official Language are: (en)(es)(fr)(ru)(ar)(zh)
			
//			logger.debug("searchByModeLangScopeXML: number of results from search = "+retrievedResources.size());
			
			if(format.compareToIgnoreCase("skos") == 0) {
				// Create a SPARQL query with all the retrieve result
				
				if(retrievedResources.size() == 0){
					/*query = "CONSTRUCT {?x ?x ?x .}"+
							"\nWHERE{" +
							"\n?x ?x ?x . " +
							"\nFILTER(1 > 2)" +
							"\n}";*/
					// Do nothing since no result was found using the indexes
				} else{
					String query = "";
					query = "CONSTRUCT {" +
							"\n?conceptURI <"+TYPE+"> <"+CONCEPT+"> . " + 
							"\n?conceptURI <"+PREFLABEL+"> ?prefLabel . " +
							"\n?conceptURI <"+ALTLABEL+"> ?altLabel . " +
							"\n?conceptURI <"+HIDDENLABEL+"> ?hiddenLabel . " +
							"\n}";
					
					
					query +="\nWHERE { ";
					Iterator<ARTLiteral> iter = retrievedResources.iterator();
					
					query +="\nFILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						ARTLiteral artLiteral = iter.next().asLiteral();
						query += "\n?label = \""+artLiteral.getLabel()+"\"@"+artLiteral.getLanguage();
					}
					query+=")";
					
					if(searchMode.toLowerCase().contains("word")){ // exact word
						query += "\nFILTER regex(str(?label), \""+searchString+"\" , \"i\")";
					} else if(searchMode.toLowerCase().contains("exact")){ // exact match
						query += "\nFILTER regex(str(?label), \"^"+searchString+"$\" , \"i\")";
					} else if(searchMode.toLowerCase().contains("start")){
						query += "\nFILTER regex(str(?label), \"^"+searchString+".*\" , \"i\")";
					} else if(searchMode.toLowerCase().contains("contain")){
						query += "\nFILTER regex(str(?label), \".*"+searchString+".*\" , \"i\")";
					} else if(searchMode.toLowerCase().contains("end")){
						query += "\nFILTER regex(str(?label), \".*"+searchString+"$\", \"i\")";
					}
					
					query+=	"\n?genericXLabel <"+SKOSXLLITERALFORM+"> ?label ."+
							"\n?genericXLabel <"+NOTATION+"> ?termcode .";
							
					
					query +="\n{?prefxlabel <"+NOTATION+"> ?termcode ." +
							"\n?conceptURI <"+SKOSXLPREFLABEL+"> ?prefxlabel . " +
							"\n?prefxlabel <"+SKOSXLLITERALFORM+"> ?prefLabel . ";
							if(filterOutputWithLang && lang.compareToIgnoreCase("all") != 0){
								query +="\nFILTER langMatches(lang(?prefLabel), \""+lang+"\")";
							}
							query +="\n}"+
							
							"\nUNION"+
							
							"\n{?altXLabel <"+NOTATION+"> ?termcode ." +
							"\n?conceptURI <"+SKOSXLALTLABEL+"> ?altXLabel . " +
							"\n?altXLabel <"+SKOSXLLITERALFORM+"> ?altLabel . " ;
							if(filterOutputWithLang && lang.compareToIgnoreCase("all") != 0){
								query +="\nFILTER langMatches(lang(?altLabel), \""+lang+"\")";
							}
							query +="\n}"+
							
							"\nUNION"+
	
							"\n{?hiddenXLabel <"+NOTATION+"> ?termcode . " +
							"\n?conceptURI <"+SKOSXLHIDDENLABEL+"> ?hiddenXLabel . " +
							"\n?hiddenXLabel <"+SKOSXLLITERALFORM+"> ?hiddenLabel . ";
							if(filterOutputWithLang && lang.compareToIgnoreCase("all") != 0){
								query +="\nFILTER langMatches(lang(?altLabel), \""+lang+"\")";
							}
							query +="\n}";
							
					
					query +="\n}";
					
					printQuery(query);
					
					GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
					ARTStatementIterator itFromQuery = graphQuery.evaluate(true);
					ARTStatementIterator it = removeDuplicateSTatement(itFromQuery);
					
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
					skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
					result = out.toString();
					itFromQuery.close();
					it.close();
					out.close();
					writer.close();
				}
				
			} else {
				return DEPRECATED+" for format = "+format;
			}
			
		} catch (IndexAccessException e) {
			logger.error("searchByModeLangScopeXML", e);
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (ParseException e) {
			logger.error("searchByModeLangScopeXML", e);
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (QueryEvaluationException e) {
			logger.error("searchByModeLangScopeXML", e);
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (UnsupportedQueryLanguageException e) {
			logger.error("searchByModeLangScopeXML", e);
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (ModelAccessException e) {
			logger.error("searchByModeLangScopeXML", e);
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (MalformedQueryException e) {
			logger.error("searchByModeLangScopeXML", e);
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (UnsupportedRDFFormatException e) {
			logger.error("searchByModeLangScopeXML", e);
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (IOException e) {
			logger.error("searchByModeLangScopeXML", e);
			return errorMessage+"searchByModeLangScopeXML"; 
		}
		
		return result;
		
		//return NOTIMPLEMENTED;
	}

	public String getTermsByClassName(String ontologyName, String className, String descriptor,
			String format, String status) {
		return DEPRECATED;
	}

	public String getTermsByCodesAndLanguage(String ontologyName, String termcode, String codeName,
			String format, String lang) {
		return DEPRECATED;
	}

	public String getTermsByURI(String ontologyName, String conceptURI, String lang, String descriptor,
			String format, String status) {
		return DEPRECATED;
	}

	// TODO DONE
	/**
	 * The Method will return the list of concept URI for the given term, search filter and language. 
	 * The result of this method will be in TXT or SKOS format.
	 * @param ontologyName
	 * @param term
	 * @param searchMode contains, exact match, starts with, ends with
	 * @param format TXT or SKOS
	 * @param lang
	 * @return the list of concept URI for the given term, search filter and language
	 */
	public String getURIByTermAndLangXML(String ontologyName, String term, String searchMode, String format,
			String lang) {
//		logger.debug("getURIByTermAndLangXML:"+
//			debugSeparator+"ontologyName="+ontologyName+
//			debugSeparator+"term="+term+
//			debugSeparator+"searchMode="+searchMode+
//			debugSeparator+"format="+format+
//			debugSeparator+"lang="+lang);
		Collection<ARTURIResource> retrievedResources = null;
		String result = "";
		try {
			retrievedResources = searchConcept(term, searchMode, lang);
			
			if(retrievedResources == null)
				return DEPRECATED+" search mode: "+searchMode;
//			logger.debug("getURIByTermAndLangXML: number of results from search = "+retrievedResources.size());
			// return the result depending on the requested format
			if (format.compareToIgnoreCase("txt") == 0) {
				// Create a SPARQL query with all the retrieve result

				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				boolean first = true;
				while(iter.hasNext()){
					if(!first)
						result += " ,";
					first = false;
					result += iter.next().asURIResource().getURI();
				}
				if(result.endsWith(","))
					result = result.substring(0,result.length()-2);
			} else if (format.compareToIgnoreCase("skos") == 0){ // format == "skos"
				
				
				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					// Create a SPARQL query with all the retrieve result
					String query = "";
					query = "CONSTRUCT { ?conceptURI ?type ?conceptType  }"+
							"\nWHERE { ";
					query += "\nFILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?conceptURI = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query+="\n?conceptURI ?type ?conceptType .";
					
					query +="\nFILTER(?conceptType = <"+CONCEPT+"> )" +
							"\n}";
					printQuery(query);
					GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
					ARTStatementIterator it = graphQuery.evaluate(true);
					
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
					skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
					result = out.toString();
					it.close();
					out.close();
					writer.close();
				}
				
			} 

		} catch (IndexAccessException e) {
			e.printStackTrace();
			return errorMessage+"getURIByTermAndLangXML"; 
		} catch (ParseException e) {
			e.printStackTrace();
			return errorMessage+"getURIByTermAndLangXML"; 
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getURIByTermAndLangXML"; 
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getURIByTermAndLangXML"; 
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getURIByTermAndLangXML"; 
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getURIByTermAndLangXML"; 
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
			return errorMessage+"getURIByTermAndLangXML"; 
		} catch (IOException e) {
			e.printStackTrace();
			return errorMessage+"getURIByTermAndLangXML"; 
		}
		return result;
	}

	public String reloadOWLModel(String ontologyName) {
		return DEPRECATED;
	}

	public String getListofURI(String ontologyName, String uriStart, String numResult) {
		return DEPRECATED;
	}

	// TODO DONE
	/**
	 * The Method will return the concept by keyword, and can filter with search mode and language. 
	 * The result of this method will be in TXT or SKOS format.
	 * @param ontologyName
	 * @param searchString
	 * @param format TXT, SKOS, URI-txt
	 * @param searchMode contains, exact match, starts with, ends with
	 * @param lang
	 * @return the concept by keyword, and can filter with search mode and language
	 */
	public String getConceptByKeyword(String ontologyName, String searchString, String format, String searchMode, 
			String lang) {
		logger.debug("getConceptByKeyword:"+
				debugSeparator+"ontologyName="+ontologyName+
				debugSeparator+"searchString="+searchString+
				debugSeparator+"format="+format+
				debugSeparator+"searchMode="+searchMode+
				debugSeparator+"lang="+lang);
		Collection<ARTURIResource> retrievedResources = null;
		String result = "";
		try {
			retrievedResources = searchConcept(searchString, searchMode, lang);
			
			if(retrievedResources == null)
				return DEPRECATED+" search mode: "+searchMode;
			logger.debug("getConceptByKeyword: number of results from search = "+retrievedResources.size());
			// return the result depending on the requested format
			if (format.compareToIgnoreCase("txt") == 0) {
				result = "[";
				
				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				
				//query += "FILTER(";
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					// Create a SPARQL query with all the retrieve result
					String query = "SELECT ?conceptURI ?label "+ 
							"\nWHERE { "+
							"\n ?conceptURI ?pred1 _:b1 . ";
					query += "FILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?conceptURI = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query +="\n FILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
							"?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"?pred1 = <"+SKOSXLPREFLABEL+"> ) ."+
							"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label ."+
							"}" +
							"\nORDER BY ?conceptURI";
					printQuery(query);
					
					TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
					TupleBindingsIterator it = tupleQuery.evaluate(true);
					
					String lastConceptURI = "";
					while(it.hasNext()){
						TupleBindings tuple = it.next();
						Iterator<Binding> iterInTuple = tuple.iterator();
						String conceptId = iterInTuple.next().getBoundValue().asURIResource().getLocalName();
						if(lastConceptURI.compareToIgnoreCase(conceptId) != 0) {// new concept
							lastConceptURI = conceptId;
							if(result.length()>1){
								result += "][";
							}
						}
						else if(result.length()>1)
							result +=",";
						ARTLiteral literal = iterInTuple.next().getBoundValue().asLiteral();
						result += literal.getLabel()+" ("+literal.getLanguage()+")";
					}
					it.close();
				}
				
				result +="]";
			} else if (format.compareToIgnoreCase("skos") == 0){ // format == "skos"
				
				
				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					// Create a SPARQL query with all the retrieve result
					String query = "";
					query = "CONSTRUCT { ?conceptURI ?pred1 ?label . " +
							"\n ?conceptURI ?pred2 ?obj2 ." +
							"\n ?obj2 ?pred3 ?label3 }"+
							"\nWHERE { ";
					query += "FILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?conceptURI = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query +="\n{ "+
							"\n ?conceptURI ?pred1 _:b1 . "+
							"\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
							"\n?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"\n?pred1 = <"+SKOSXLPREFLABEL+"> ) . "+
							"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label . "+
							"\n} "+
							"\nUNION { "+
							"\n ?conceptURI ?pred2 ?obj2 . "+
							"\nFILTER(?pred2 != <"+SKOSXLALTLABEL+"> && "+
							"\n?pred2 != <"+SKOSXLHIDDENLABEL+"> &&  "+
							"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
							"\n?pred2 != <"+LABEL+"> && "+
							"\n?pred2 != <"+PREFLABEL+"> && "+
							"\n?pred2 != <"+ALTLABEL+"> && "+
							"\n?pred2 != <"+HIDDENLABEL+"> && "+
							"\n?pred2 != <"+SEMANTICRELATION+">) . "+
							"\nOPTIONAL{ "+
							"\n	?obj2 a <"+CONCEPT+"> . "+
							"\n	?obj2 ?predTemp _:b3 . "+
							"\n	FILTER(?predTemp = <"+SKOSXLALTLABEL+"> || "+
							"\n?predTemp = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"\n?predTemp = <"+SKOSXLPREFLABEL+"> ) . "+
							"\n	?obj2 ?pred3 _:b3 . "+
							"\n	_:b3 <"+SKOSXLLITERALFORM+"> ?label3 . "+
							"\n} "+
							"\n} "+
							"\n}" +
							"\nORDER BY ?conceptURI";
					printQuery(query);
					
					GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
					ARTStatementIterator it = graphQuery.evaluate(true);
					
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
					skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
					result = out.toString();
					it.close();
					out.close();
					writer.close();
				}
				
			} else { // format == URI-txt
				
				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					// Create a SPARQL query with all the retrieve result
					String query = "";
					query = "CONSTRUCT { ?conceptURI ?pred1 ?label . " +
							"\n ?conceptURI ?pred2 ?obj2 .}"+
							"\nWHERE { ";
					query += "FILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?conceptURI = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query +="\n{ "+
							"\n ?conceptURI ?pred1 _:b1 . "+
							"\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
							"\n?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"\n?pred1 = <"+SKOSXLPREFLABEL+"> ) . "+
							"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label . "+
							"\n} "+
							"\nUNION { "+
							"\n ?conceptURI ?pred2 ?obj2 . "+
							"\nFILTER(?pred2 != <"+SKOSXLALTLABEL+"> && "+
							"\n?pred2 != <"+SKOSXLHIDDENLABEL+"> &&  "+
							"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
							"\n?pred2 != <"+LABEL+"> && "+
							"\n?pred2 != <"+PREFLABEL+"> && "+
							"\n?pred2 != <"+ALTLABEL+"> && "+
							"\n?pred2 != <"+HIDDENLABEL+"> && "+
							"\n?pred2 != <"+SEMANTICRELATION+">) . "+
							"\n} "+
							"\n}" +
							"\nORDER BY ?conceptURI";
					printQuery(query);
					
					GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
					ARTStatementIterator it = graphQuery.evaluate(true);
					result = "";

					String lastConcept = "";
					while(it.hasNext()){
						ARTStatement stat = it.next();
						ARTURIResource subj = stat.getSubject().asURIResource();
						ARTURIResource pred = stat.getPredicate();
						ARTNode obj = stat.getObject();
						if(lastConcept.compareTo(subj.getURI()) != 0){ // new concept
							lastConcept = subj.getURI();
							result += "URI: "+subj.getURI()+"\n";
							
						}
						result += pred.getLocalName()+": ";
						if(obj.isLiteral()){
							ARTLiteral objLiteral = obj.asLiteral();
							result  += objLiteral.getLabel();
							if(objLiteral.getLanguage() != null && objLiteral.getLanguage().compareTo("")!=0)
								result  += " ("+objLiteral.getLanguage()+")";
						}
						else if(obj.isURIResource()){
							ARTURIResource objURIRes = obj.asURIResource();
							result += objURIRes.getURI();
						}
						result += "\n";
					}
					it.close();
				}
				
			}

		} catch (IndexAccessException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword"; 
		} catch (ParseException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword"; 
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword"; 
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword"; 
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword"; 
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword"; 
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword"; 
		} catch (IOException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword"; 
		}
		return result;
	}

	
	// TODO DONE
		/**
		 * The Method will return the concept by keyword, and can filter with search mode and language. 
		 * The result of this method will be in TXT or SKOS format.
		 * @param ontologyName
		 * @param searchString
		 * @param format TXT, SKOS, URI-txt
		 * @param searchMode contains, exact match, starts with, ends with
		 * @param lang
		 * @param outlang
		 * @return the concept by keyword, and can filter with search mode and language
		 */
	public String getConceptByKeyword2(String ontologyName, String searchString, String format, String searchMode, 
				String lang, String outlang) {
//		logger.debug("getConceptByKeyword2:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"searchString="+searchString+
//				debugSeparator+"format="+format+
//				debugSeparator+"searchMode="+searchMode+
//				debugSeparator+"lang="+lang+
//				debugSeparator+"outlang="+outlang);
		Collection<ARTURIResource> retrievedResources = null;
		String result = "";
		try {
			retrievedResources = searchConcept(searchString, searchMode, lang);
			
			if(retrievedResources == null)
				return DEPRECATED+" search mode: "+searchMode;
//			logger.debug("getConceptByKeyword2: number of results from search = "+retrievedResources.size());
			// return the result depending on the requested format
			if (format.compareToIgnoreCase("txt") == 0) {

				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				result = "[";
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					// Create a SPARQL query with all the retrieve result
					String query = "SELECT ?conceptURI ?label "+ 
							"\nWHERE { "+
							"\n ?conceptURI ?pred1 _:b1 . ";
					query += "FILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?conceptURI = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query +="\n FILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
							"?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"?pred1 = <"+SKOSXLPREFLABEL+"> ) ."+
							"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label .";
					if(outlang.compareToIgnoreCase("all") != 0)
						query +="\nFILTER( langMatches(lang(?label), \""+outlang+"\"))";
					query += "}" +
							"\nORDER BY ?conceptURI";
					printQuery(query);
					
					TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
					TupleBindingsIterator it = tupleQuery.evaluate(true);
					
					String lastConceptURI = "";
					while(it.hasNext()){
						TupleBindings tuple = it.next();
						Iterator<Binding> iterInTuple = tuple.iterator();
						String conceptId = iterInTuple.next().getBoundValue().asURIResource().getLocalName();
						if(lastConceptURI.compareToIgnoreCase(conceptId) != 0) {// new concept
							lastConceptURI = conceptId;
							if(result.length()>1){
								result += "][";
							}
						}
						else if(result.length()>1)
							result +=",";
						ARTLiteral literal = iterInTuple.next().getBoundValue().asLiteral();
						result += literal.getLabel()+" ("+literal.getLanguage()+")";
					}
					it.close();
					
				}
				result +="]";
			} else if (format.compareToIgnoreCase("skos") == 0){ // format == "skos"
				
				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					// Create a SPARQL query with all the retrieve result
					String query = "";
					query = "CONSTRUCT { ?conceptURI ?pred1 ?label . " +
							"\n ?conceptURI ?pred2 ?obj2 ." +
							"\n ?obj2 ?pred3 ?label3 }"+
							"\nWHERE { ";
					query += "FILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?conceptURI = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query +="\n{ "+
							"\n ?conceptURI ?pred1 _:b1 . "+
							"\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
							"\n?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"\n?pred1 = <"+SKOSXLPREFLABEL+"> ) . "+
							"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label . ";
					if(outlang.compareToIgnoreCase("all") != 0)
						query +="\nFILTER( langMatches(lang(?label), \""+outlang+"\"))";
					query +="\n} "+
							"\nUNION { "+
							"\n ?conceptURI ?pred2 ?obj2 . "+
							"\nFILTER(?pred2 != <"+SKOSXLALTLABEL+"> && "+
							"\n?pred2 != <"+SKOSXLHIDDENLABEL+"> &&  "+
							"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
							"\n?pred2 != <"+LABEL+"> && "+
							"\n?pred2 != <"+PREFLABEL+"> && "+
							"\n?pred2 != <"+ALTLABEL+"> && "+
							"\n?pred2 != <"+HIDDENLABEL+"> && "+
							"\n?pred2 != <"+SEMANTICRELATION+">) . "+
							"\nOPTIONAL{ "+
							"\n	?obj2 a <"+CONCEPT+"> . "+
							"\n	?obj2 ?predTemp _:b3 . "+
							"\n	FILTER(?predTemp = <"+SKOSXLALTLABEL+"> || "+
							"\n?predTemp = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"\n?predTemp = <"+SKOSXLPREFLABEL+"> ) . "+
							"\n	?obj2 ?pred3 _:b3 . "+
							"\n	_:b3 <"+SKOSXLLITERALFORM+"> ?label3 . ";
					if(outlang.compareToIgnoreCase("all") != 0)
						query +="\nFILTER( langMatches(lang(?label3), \""+outlang+"\"))";
					query += "\n} "+
							"\n} "+
							"\n}" +
							"\nORDER BY ?conceptURI";
					printQuery(query);
					
					GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
					ARTStatementIterator it = graphQuery.evaluate(true);
					
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
					skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
					result = out.toString();
					it.close();
					out.close();
					writer.close();
				}
			} else { // format == URI-txt
				
				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					// Create a SPARQL query with all the retrieve result
					String query = "";
					query = "CONSTRUCT { ?conceptURI ?pred1 ?label . " +
							"\n ?conceptURI ?pred2 ?obj2 .}"+
							"\nWHERE { ";
					query += "FILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?conceptURI = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query +="\n{ "+
							"\n ?conceptURI ?pred1 _:b1 . "+
							"\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
							"\n?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"\n?pred1 = <"+SKOSXLPREFLABEL+"> ) . "+
							"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label . ";
					if(outlang.compareToIgnoreCase("all") != 0)
						query +="\nFILTER( langMatches(lang(?label), \""+outlang+"\"))";
					query += "\n} "+
							"\nUNION { "+
							"\n ?conceptURI ?pred2 ?obj2 . "+
							"\nFILTER(?pred2 != <"+SKOSXLALTLABEL+"> && "+
							"\n?pred1 != <"+SKOSXLHIDDENLABEL+"> &&  "+
							"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
							"\n?pred2 != <"+LABEL+"> && "+
							"\n?pred2 != <"+PREFLABEL+"> && "+
							"\n?pred2 != <"+ALTLABEL+"> && "+
							"\n?pred2 != <"+HIDDENLABEL+"> && "+
							"\n?pred2 != <"+SEMANTICRELATION+">) . "+
							"\n} "+
							"\n}" +
							"\nORDER BY ?conceptURI";
					
					
					GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
					ARTStatementIterator it = graphQuery.evaluate(true);
					result = "";

					String lastConcept = "";
					while(it.hasNext()){
						ARTStatement stat = it.next();
						ARTURIResource subj = stat.getSubject().asURIResource();
						ARTURIResource pred = stat.getPredicate();
						ARTNode obj = stat.getObject();
						if(lastConcept.compareTo(subj.getURI()) != 0){ // new concept
							lastConcept = subj.getURI();
							result += "URI: "+subj.getURI()+"\n";
							
						}
						result += pred.getLocalName()+": ";
						if(obj.isLiteral()){
							ARTLiteral objLiteral = obj.asLiteral();
							result  += objLiteral.getLabel();
							if(objLiteral.getLanguage() != null && objLiteral.getLanguage().compareTo("")!=0)
								result  += " ("+objLiteral.getLanguage()+")";
						}
						else if(obj.isURIResource()){
							ARTURIResource objURIRes = obj.asURIResource();
							result += objURIRes.getURI();
						}
						result += "\n";
					}
					it.close();
				}
			}
		} catch (IndexAccessException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword2"; 
		} catch (ParseException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword2"; 
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword2"; 
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword2"; 
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword2"; 
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword2"; 
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword2"; 
		} catch (IOException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByKeyword2"; 
		}
		return result;
	}
		

	public String simpleSearchByModeLangXML(String ontologyName, String searchString, String format, 
			String searchMode, String lang, String []outlang) {
		return searchByModeLangScopeXML(ontologyName, searchString, format, searchMode, lang, outlang, 
				"all");
		//return NOTIMPLEMENTED;
	}

	// TODO DONE
	/**
	 * The Method returns the output in TXT or SKOS format containing the specified search string in the 
	 * specified search mode, language, scope and output language
	 * @param ontologyName
	 * @param searchString
	 * @param format TXT or SKOS
	 * @param searchMode contains, exact match, starts with, ends with
	 * @param lang
	 * @param outputLang
	 * @param scopeid
	 * @return the output in TXT or SKOS format containing the specified search string in the 
	 * specified search mode, language, scope and output language
	 */
	public String searchByModeLangScopeXML(String ontologyName, String searchString, String format, 
			String searchMode,	String lang, String[] outlang, String scopeid) {
//		logger.debug("searchByModeLangScopeXML:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"searchString="+searchString+
//				debugSeparator+"format="+format+
//				debugSeparator+"searchMode="+searchMode+
//				debugSeparator+"lang="+lang+
//				debugSeparator+"outlang="+Arrays.toString(outlang)+
//				debugSeparator+"outlang="+scopeid);
		Collection<ARTURIResource> retrievedResources = null;
		String result = "";
		
		// the possible values for scopeid are: "All terms", "Only geographical terms", 
		//"Only Taxonomic terms", "All except greographical terms"
		
		try {
			
			retrievedResources = searchConcept(searchString, searchMode, lang);
			
			if(retrievedResources == null)
				return DEPRECATED+" search mode: "+searchMode;
			// return the result depending on the requested format
			// check the output language, the FAO Official Language are: (en)(es)(fr)(ru)(ar)(zh)
			
//			logger.debug("searchByModeLangScopeXML: number of results from search = "+retrievedResources.size());
			
			if (format.compareToIgnoreCase("txt") == 0) {

				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				result = "[";
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					// Create a SPARQL query with all the retrieve result
					String query = "SELECT ?conceptURI ?label "+ 
							"\nWHERE { "+
							"\n ?conceptURI ?pred1 _:b1 . ";
					query += "\nFILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?conceptURI = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					query +="\n FILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
							"?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
							"?pred1 = <"+SKOSXLPREFLABEL+"> ) ."+
							"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label .";
					
					//put a filter depending on the output language
					boolean noFilter = false;
					for(int i=0; i<outlang.length;++i){
						if(outlang[i].compareToIgnoreCase("all") == 0){
							// use no filter
							noFilter = true;
						}
					} 
					if(!noFilter){
						query += "\n FILTER(";
						for(int i=0; i<outlang.length; ++i){
							if(i>0)
								query += " || ";
							if(outlang[i].toLowerCase().startsWith("fao") ){
								query += "\nlangMatches(lang(?label), \"en\") ||+" +
										"langMatches(lang(?label), \"es\") ||"+
										"langMatches(lang(?label), \"fr\") ||"+
										"langMatches(lang(?label), \"ru\") ||"+
										"langMatches(lang(?label), \"ar\") ||"+
										"langMatches(lang(?label), \"zh\") ";
							} else {
									query += "\nlangMatches(lang(?label), \""+outlang[i]+"\")";
							}
						}
						query += ")";
					}
					//filter accordingly to the scopeid value
					if(scopeid.toLowerCase().startsWith("all except")){ // All except geographical terms
						query += "\n ?conceptURI <"+ISPARTOFSSUBVOCABULARY+"> ?valueSubVoc ." +
								"\nFILTER( str(?valueSubVoc) != \"Geographical below country level\" && "+ 
								"\n str(?valueSubVoc) != \"Geographical above country level\" && " +
								"\n str(?valueSubVoc) != \"Geographical country level\" )";
					} else if(scopeid.toLowerCase().startsWith("only geographical") ){ // Only geographical terms
						query += "\n ?conceptURI <"+ISPARTOFSSUBVOCABULARY+"> ?valueSubVoc ." +
								"\nFILTER( str(?valueSubVoc) = \"Geographical below country level\" || "+ 
								"\n str(?valueSubVoc) = \"Geographical above country level\" || " +
								"\n str(?valueSubVoc) = \"Geographical country level\" )";
					} else if(scopeid.toLowerCase().startsWith("only taxonomic") ){ // Only Taxonomic terms
						query += "\n _:b1 <"+HASTERMTYPE+"> ?valueTermType ." +
								"\nFILTER( str(?valueSubVoc) != \"\" || "+ 
								"\n str(?valueTermType) != \"Taxonomic terms for animals\" || " +
								"\n str(?valueTermType) != \"Taxonomic terms for bacteria\" || " +
								"\n str(?valueTermType) != \"Taxonomic terms for fungi\" || " +
								"\n str(?valueTermType) != \"Taxonomic terms for plants\" || " +
								"\n str(?valueTermType) != \"Taxonomic terms for viruses\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (animals)\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (bacteria)\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (fungi)\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (plants)\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (viruses)\" ) ";
					} else { // all terms
						// no filter is needed
					}
					query +="}";
					printQuery(query);
					
					TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
					TupleBindingsIterator it = tupleQuery.evaluate(true);
					
					
					String lastConceptURI = "";
					while(it.hasNext()){
						TupleBindings tuple = it.next();
						Iterator<Binding> iterInTuple = tuple.iterator();
						String conceptId = iterInTuple.next().getBoundValue().asURIResource().getLocalName();
						if(lastConceptURI.compareToIgnoreCase(conceptId) != 0) {// new concept
							lastConceptURI = conceptId;
							if(result.length()>1){
								result += "][";
							}
						}
						else if(result.length()>1)
							result +=",";
						ARTLiteral literal = iterInTuple.next().getBoundValue().asLiteral();
						result += literal.getLabel()+" ("+literal.getLanguage()+")";
					}
					it.close();
					
				}
				result +="]";
			} else { // format == "skos"
				
				Iterator<ARTURIResource> iter = retrievedResources.iterator();
				
				if(retrievedResources.size() == 0){
					//query += "1 = 2";
					//do nothing and do not do any SPARQL query
				}
				else{
					// Create a SPARQL query with all the retrieve result
					String query = "";
					query = //"CONSTRUCT { ?conceptURI ?pred1 ?label . " +
							"CONSTRUCT {" +
							"\n?conceptURI <"+TYPE+"> <"+CONCEPT+"> . " + 
							"\n?conceptURI <"+PREFLABEL+"> ?prefLabel . " +
							"\n?conceptURI <"+ALTLABEL+"> ?altLabel . " +
							"\n?conceptURI <"+HIDDENLABEL+"> ?hiddenLabel . " +
							"\n?conceptURI ?pred2 ?obj2 . " +
							"\n}"+
							"\nWHERE { "+
							"\n?conceptURI <"+TYPE+"> <"+CONCEPT+"> . " ;
					query += "\nFILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						query += "\n?conceptURI = <"+iter.next().asURIResource().getURI()+">";
					}
					query += ")";
					
					/*boolean first = true;
					for(ARTURIResource conceptURI : retrievedResources){
						if(!first)
							query += "\nUNION";
						else
							first = false;
						query +="\n{BIND (<"+conceptURI.getURI()+"> AS ?conceptURI) }";
					}*/
					
					
					/*query +="\n?conceptURI ?pred1 _:b1 . "+
							"\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
							"\n?pred1 = <"+SKOSXLPREFLABEL+"> ) . "+
							"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label . ";*/
					
					//put a filter depending on the output language
					boolean noFilter = false;
					for(int i=0; i<outlang.length; ++i){
						if(outlang[i].compareToIgnoreCase("all") == 0){
							// use no filter
							noFilter = true;
						} 
					}
					
					query +="\n" +
							"\n?conceptURI <"+SKOSXLPREFLABEL+"> ?prefXLabel ."+
							"\n?prefXLabel <"+SKOSXLLITERALFORM+"> ?prefLabel . ";
					if(!noFilter){
						query += "\nFILTER(";
						for(int i=0; i<outlang.length; ++i){
							if(i>0){
								query += " || ";
							}
							if(outlang[i].toLowerCase().startsWith("fao") ){
								query += "\nlangMatches(lang(?prefLabel), \"en\") ||+" +
										"langMatches(lang(?prefLabel), \"es\") ||"+
										"langMatches(lang(?prefLabel), \"fr\") ||"+
										"langMatches(lang(?prefLabel), \"ru\") ||"+
										"langMatches(lang(?prefLabel), \"ar\") ||"+
										"langMatches(lang(?prefLabel), \"zh\") ";
							} else {
									query += "\nlangMatches(lang(?prefLabel), \""+outlang[i]+"\")";
							}
						}
						query += "\n)";
					}
					query +="\n"+
							"\nOPTIONAL{?conceptURI <"+SKOSXLALTLABEL+"> ?altXLabel . "+
							"\n?altXLabel <"+SKOSXLLITERALFORM+"> ?altLabel . ";
					if(!noFilter){
						query += "\nFILTER(";
						for(int i=0; i<outlang.length; ++i){
							if(i>0){
								query += " || ";
							}
							if(outlang[i].toLowerCase().startsWith("fao") ){
								query += "\nlangMatches(lang(?altLabel), \"en\") ||+" +
										"langMatches(lang(?altLabel), \"es\") ||"+
										"langMatches(lang(?altLabel), \"fr\") ||"+
										"langMatches(lang(?altLabel), \"ru\") ||"+
										"langMatches(lang(?altLabel), \"ar\") ||"+
										"langMatches(lang(?altLabel), \"zh\") ";
							} else {
									query += "\nlangMatches(lang(?altLabel), \""+outlang[i]+"\")";
							}
						}
						query += "\n)";
					}
					query+="\n}";
					query +="\n"+
							"\nOPTIONAL{?conceptURI <"+SKOSXLHIDDENLABEL+"> ?hiddenXLabel . "+
							"\n?hiddenXLabel <"+SKOSXLLITERALFORM+"> ?hiddenLabel . ";
					if(!noFilter){
						query += "\nFILTER(";
						for(int i=0; i<outlang.length; ++i){
							if(i>0){
								query += " || ";
							}
							if(outlang[i].toLowerCase().startsWith("fao") ){
								query += "\nlangMatches(lang(?hiddenLabel), \"en\") ||+" +
										"langMatches(lang(?hiddenLabel), \"es\") ||"+
										"langMatches(lang(?hiddenLabel), \"fr\") ||"+
										"langMatches(lang(?hiddenLabel), \"ru\") ||"+
										"langMatches(lang(?hiddenLabel), \"ar\") ||"+
										"langMatches(lang(?hiddenLabel), \"zh\") ";
							} else {
									query += "\nlangMatches(lang(?hiddenLabel), \""+outlang[i]+"\")";
							}
						}
						query += "\n)";
					}
					query+="\n}";
					

					query +=
							"\nOPTIONAL{"+
							"\n ?conceptURI ?pred2 ?obj2 . "+
							"\nFILTER( (?pred2 != <"+SKOSXLALTLABEL+"> && "+
							"\n?pred2 != <"+SKOSXLHIDDENLABEL+"> &&  "+
							"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
							"\n?pred2 != <"+LABEL+"> && "+
							"\n?pred2 != <"+PREFLABEL+"> && "+
							"\n?pred2 != <"+ALTLABEL+"> && "+
							"\n?pred2 != <"+HIDDENLABEL+"> && "+
							"\n?pred2 != <"+SEMANTICRELATION+"> && "+
							"\n?pred2 != <"+BROADERTRANSIVE+"> && "+
							"\n?pred2 != <"+NARROWERTRANSIVE+"> &&"+
							"\n?pred2 != <"+TYPE+"> ) ||" +
							"\n(?pred2 = <"+TYPE+"> && ?obj2 = <"+CONCEPT+"> )"+
							") . "+
							"";
					query +="\n} ";
					//filter accordingly to the scopeid value
					if(scopeid.toLowerCase().startsWith("all except")){ // All except greographical terms
						query += "\n ?conceptURI <"+ISPARTOFSSUBVOCABULARY+"> ?valueSubVoc ." +
								"\nFILTER( str(?valueSubVoc) != \"Geographical below country level\" && "+ 
								"\n str(?valueSubVoc) != \"Geographical above country level\" && " +
								"\n str(?valueSubVoc) != \"Geographical country level\" )";
					} else if(scopeid.toLowerCase().startsWith("only geographical") ){ // Only geographical terms
						query += "\n ?conceptURI <"+ISPARTOFSSUBVOCABULARY+"> ?valueSubVoc ." +
								"\nFILTER( str(?valueSubVoc) = \"Geographical below country level\" || "+ 
								"\n str(?valueSubVoc) = \"Geographical above country level\" || " +
								"\n str(?valueSubVoc) = \"Geographical country level\" )";
					} else if(scopeid.toLowerCase().startsWith("only taxonomic") ){ // Only Taxonomic terms
						query += "\n _:b1 <"+HASTERMTYPE+"> ?valueTermType ." +
								"\nFILTER( str(?valueSubVoc) != \"\" || "+ 
								"\n str(?valueTermType) != \"Taxonomic terms for animals\" || " +
								"\n str(?valueTermType) != \"Taxonomic terms for bacteria\" || " +
								"\n str(?valueTermType) != \"Taxonomic terms for fungi\" || " +
								"\n str(?valueTermType) != \"Taxonomic terms for plants\" || " +
								"\n str(?valueTermType) != \"Taxonomic terms for viruses\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (animals)\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (bacteria)\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (fungi)\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (plants)\" || " +
								"\n str(?valueTermType) != \"Taxonomic Terms (viruses)\" ) ";
					} else { // all terms
						// no filter is needed
					}
					query+="\n}";
					printQuery(query);
					
					GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
					ARTStatementIterator itFromQuery = graphQuery.evaluate(true);
					ARTStatementIterator it = removeDuplicateSTatement(itFromQuery);
					
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
					skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
					result = out.toString();
					itFromQuery.close();
					it.close();
					out.close();
					writer.close();
				}
			} 
			
		} catch (IndexAccessException e) {
			logger.error("searchByModeLangScopeXML", e);
			//e.printStackTrace();
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (ParseException e) {
			logger.error("searchByModeLangScopeXML", e);
			//e.printStackTrace();
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (QueryEvaluationException e) {
			logger.error("searchByModeLangScopeXML", e);
			//e.printStackTrace();
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (UnsupportedQueryLanguageException e) {
			logger.error("searchByModeLangScopeXML", e);
			//e.printStackTrace();
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (ModelAccessException e) {
			logger.error("searchByModeLangScopeXML", e);
			//e.printStackTrace();
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (MalformedQueryException e) {
			logger.error("searchByModeLangScopeXML", e);
			//e.printStackTrace();
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (UnsupportedRDFFormatException e) {
			logger.error("searchByModeLangScopeXML", e);
			//e.printStackTrace();
			return errorMessage+"searchByModeLangScopeXML"; 
		} catch (IOException e) {
			logger.error("searchByModeLangScopeXML", e);
			//e.printStackTrace();
			return errorMessage+"searchByModeLangScopeXML"; 
		}
		
		return result;
	}

	public String getAllTermInSubClass(String clsName, String OntolgoyDatabase) {
		return DEPRECATED;
		
		// To delete, used only in testing. Remembere to delete/comment, because it can be dangerouse to 
		// execute any kind of SPARQL query without any form of pre-validation 
		/*		String result = "";
				try {
					
					String query = OntolgoyDatabase;
					System.out.println("query = "+query); // DEBUG
					TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
					TupleBindingsIterator it = tupleQuery.evaluate(true);
					int cont = 0;
					while(it.hasNext()){
						String nominalValue = it.next().iterator().next().getBoundValue().getNominalValue();
						result +=nominalValue+"\n";
						++cont;
					}
					result += "\nNumber of result = "+cont;
				} catch (UnsupportedQueryLanguageException e) {
					e.printStackTrace();
				} catch (ModelAccessException e) {
					e.printStackTrace();
				} catch (MalformedQueryException e) {
					e.printStackTrace();
				} catch (QueryEvaluationException e) {
					e.printStackTrace();
				}
				
				return result;
				*/
	}

	public String getConceptRelationshipList(String clsName, String OntolgoyDatabase) {
		return DEPRECATED;
	}

	public String getConceptObject(String clsName, String OntolgoyDatabase) {
		return DEPRECATED;
	}

	// TODO DONE
	public String getFullAuthority(String ontologyName, String searchString, String searchMode, String lang,
			String format) {
//		logger.debug("getFullAuthority:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"searchString="+searchString+
//				debugSeparator+"searchMode="+searchMode+
//				debugSeparator+"lang="+lang+
//				debugSeparator+"format="+format);
		return getConceptByKeyword(ontologyName, searchString, format, searchMode, lang);
	}

	// TODO DONE
	/**
	 * The web service will allow extracting all records that have changed for given period of time. 
	 * It will be used to run the Reconciliation procedure.
	 * @param ontologyName
	 * @param dateFrom in the form dd/mm/yyyy
	 * @param dateTo in the form dd/mm/yyyy
	 * @param format TXT or SKOS
	 * @return all records that have changed for given period of time
	 */
	public String getLatestUpdates(String ontologyName, String dateFrom, String dateTo, String format) {
		// hasDateLastUpdated and hasDateCreated
		// do a query sparql to obtain the value for the property hasDateLastUpdated and hasDateCreated if
		
		// the input date is in the format dd/mm/yyyy and it should be converted in 
		//"yyyy-mm-ddT00:00:00Z"^^xsd:dateTime
//		logger.debug("getLatestUpdates:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"dateFrom="+dateFrom+
//				debugSeparator+"dateTo="+dateTo);

		String result = "";
		String startDate;
		String endDate;
		String[] startDateArray = dateFrom.split("/");
		String[] endDateArray = dateTo.split("/");
		
		if(startDateArray.length!=3 || endDateArray.length != 3)
			return "Please specify a dateFrom and a dateTo in the format DD/MM/YYYY";

		startDate = "\""+startDateArray[2]+"-"+startDateArray[1]+"-"+startDateArray[0]+
				"T00:00:00Z\"^^<"+DATETIME+">";
		endDate = "\""+endDateArray[2]+"-"+endDateArray[1]+"-"+endDateArray[0]+
				"T00:00:00Z\"^^<"+DATETIME+">";
		
		//String hasDateLastUpdated = "<http://aims.fao.org/aos/agrontology#hasDateLastUpdated>";
		//String hasDateCreated = "<http://aims.fao.org/aos/agrontology#hasDateCreated>";
		try {
			if (format.compareToIgnoreCase("txt") == 0) {
				// prepare a SPARQL query which retrieve all the terms for which a term have changed in the 
				// request time interval 
				String query = "SELECT ?concept ?label " +
						"\nWHERE {" +
						"\n?concept ?pred ?xlabel ." +
						"\nFILTER( ?pred=<"+SKOSXLPREFLABEL+"> || " +
						"?pred = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"?pred = <"+SKOSXLALTLABEL+">)" +
						"\n?xlabel <"+SKOSXLLITERALFORM+"> ?label ." +
						"\n?concept <"+HASDATECREATED+"> ?created ."+
						"\nOPTIONAL{ ?concept <"+HASDATEUPDATED+"> ?updated .}"+
						//"\n?xlabel <"+HASDATECREATED+"> ?created ."+
						//"\nOPTIONAL{ ?xlabel <"+HASDATEUPDATED+"> ?updated .}"+
						"\nFILTER(" +
						"\n(!bound(?updated) " +
						"\n&& <"+DATETIME+">(?created) >"+startDate+
						"\n&& <"+DATETIME+">(?created) <"+endDate+" )" +
						"\n||" +
						"\n(bound(?updated) " +
						"\n&& <"+DATETIME+">(?updated) >"+startDate+
						"\n&& <"+DATETIME+">(?updated) <"+endDate+")" +
						"\n)" +
						"\n}" +
						"\nORDER BY ?concept";
				printQuery(query);
				
				TupleQuery tupleQuery = (TupleQuery) skosxlModel.createQuery(QueryLanguage.SPARQL, query);
				TupleBindingsIterator tupleBindIter = tupleQuery.evaluate(true);
				
				String lastConcept = "";
				result = "[";
				while(tupleBindIter.hasNext()){
					TupleBindings tuple = tupleBindIter.next();
					Iterator<Binding> iter = tuple.iterator();
					String currentConcept = iter.next().getBoundValue().asURIResource().getLocalName();
					ARTLiteral literal = iter.next().getBoundValue().asLiteral();
					if(currentConcept.compareTo(lastConcept) != 0){ // new concept
						//++cont;
						if(result.length()!=1)
							result = result.substring(0, result.length()-2)+"][";
						lastConcept = currentConcept;
					}
					result += literal.getLabel()+" ("+literal.getLanguage()+"), ";
				}
				tupleBindIter.close();
				if(result.length()>2)
					result = result.substring(0, result.length()-2);
				result +="]";
				
			} else if (format.compareToIgnoreCase("skos") == 0){ // format == "skos"
				// prepare a SPARQL query which retrieve all the information regarding the concepts 
				// for which a term have changed in the request time interval 
				/*String query = "SELECT DISTINCT ?concept " +
						"\nWHERE {" +
						"\n?concept ?pred ?xlabel ." +
						"\nFILTER( ?pred=<"+SKOSXLPREFLABEL+"> || ?pred = <"+SKOSXLALTLABEL+">)" +
						"\n?xlabel <"+HASDATECREATED+"> ?created ."+
						"\nOPTIONAL{ ?xlabel <"+HASDATEUPDATED+"> ?updated .}"+
						"\nFILTER(" +
						"\n(!bound(?updated) " +
						"\n&& <"+DATETIME+">(?created) >"+startDate+
						"\n&& <"+DATETIME+">(?created) <"+endDate+" )" +
						"\n||" +
						"\n(bound(?updated) " +
						"\n&& <"+DATETIME+">(?updated) >"+startDate+
						"\n&& <"+DATETIME+">(?updated) <"+endDate+")" +
						"\n)" +
						"\n}";
				//System.out.println("query = "+query); // DEBUG
				TupleQuery tupleQuery = (TupleQuery) skosxlModel.createQuery(QueryLanguage.SPARQL, query);
				TupleBindingsIterator tupleBindIter = tupleQuery.evaluate(true);
				List<String> conceptList = new ArrayList<String>();
				//int cont = 0;
				while(tupleBindIter.hasNext()){
					conceptList.add(tupleBindIter.getNext().iterator().next().getBoundValue().
							asURIResource().getURI());
					//++cont;
				}
				//System.out.println("result number = "+cont); // DEBUG
				
				//now prepare a SPARQL query to retrieve the information about these concepts
				query = "CONSTRUCT { ?conceptURI ?pred1 ?label . " +
						"\n ?conceptURI ?pred2 ?obj2 ." +
						"\n ?obj2 ?pred3 ?label3 ." +
						"\n}" +
						"\nWHERE {"+
				 		"\nFILTER(";
				for(String conceptURI : conceptList){
					 query += "\n?conceptURI = <"+conceptURI+"> ||";
				}
				if(conceptList.size()>0){
					query = query.substring(0, query.length()-3);
				}
				else{
					query += " 1=2";
				}
				query +="\n)"+
						 "\n?conceptURI ?pred1 ?xlabel ." +
						"\nFILTER( ?pred1 = <"+SKOSXLPREFLABEL+"> || ?pred1 = <"+SKOSXLALTLABEL+">)" +
						"\n?xlabel <"+SKOSXLLITERALFORM+"> ?label . "+
						"\nOPTIONAL { "+
						"\n ?conceptURI ?pred2 ?obj2 . "+
						"\nFILTER(?pred2 != <"+SKOSXLALTLABEL+"> && "+
						"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
						"\n?pred2 != <"+LABEL+"> && "+
						"\n?pred2 != <"+PREFLABEL+"> && "+
						"\n?pred2 != <"+ALTLABEL+"> && "+
						"\n?pred2 != <"+SEMANTICRELATION+">) . "+
						"\nOPTIONAL{ "+
						"\n	?obj2 a <"+CONCEPT+"> . "+
						"\n	?obj2 ?pred3 _:b3 . "+
						"\n	FILTER(?pred3 = <"+SKOSXLALTLABEL+"> || "+
						"\n?pred3 = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n	_:b3 <"+SKOSXLLITERALFORM+"> ?label3 . "+
						"\n} "+
						"\n} "+
						
						"\n}";*/
				
				
				//one query which starts from the xlabel and then it retrieve all the info about the concepts
				/*String query = "CONSTRUCT { ?conceptURI ?pred1 ?label . " +
						"\n ?conceptURI ?pred2 ?obj2 ." +
						"\n ?obj2 ?pred3 ?label3 ." +
						"\n}" +
						"\nWHERE {" +
						"\n?concept ?pred ?xlabel ." +
						"\nFILTER( ?pred=<"+SKOSXLPREFLABEL+"> || ?pred = <"+SKOSXLALTLABEL+">)" +
						"\n?xlabel <"+SKOSXLLITERALFORM+"> ?label . "+
						
						"\n?xlabel <"+HASDATECREATED+"> ?created ."+
						"\nOPTIONAL{ ?xlabel <"+HASDATEUPDATED+"> ?updated .}"+
						"\nFILTER(" +
						"\n(!bound(?updated) " +
						"\n&& <"+DATETIME+">(?created) >"+startDate+
						"\n&& <"+DATETIME+">(?created) <"+endDate+" )" +
						"\n||" +
						"\n(bound(?updated) " +
						"\n&& <"+DATETIME+">(?updated) >"+startDate+
						"\n&& <"+DATETIME+">(?updated) <"+endDate+")" +
						"\n)" +
						
						"\nOPTIONAL { "+
						"\n ?conceptURI ?pred2 ?obj2 . "+
						"\nFILTER(?pred2 != <"+SKOSXLALTLABEL+"> && "+
						"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
						"\n?pred2 != <"+LABEL+"> && "+
						"\n?pred2 != <"+PREFLABEL+"> && "+
						"\n?pred2 != <"+ALTLABEL+"> && "+
						"\n?pred2 != <"+SEMANTICRELATION+">) . "+
						"\nOPTIONAL{ "+
						"\n	?obj2 a <"+CONCEPT+"> . "+
						"\n	?obj2 ?pred3 _:b3 . "+
						"\n	FILTER(?pred3 = <"+SKOSXLALTLABEL+"> || "+
						"\n?pred3 = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n	_:b3 <"+SKOSXLLITERALFORM+"> ?label3 . "+
						"\n} "+
						"\n} "+

						"\n}";*/
				
				//one query which starts from the concept and then it retrieve all the info about these concepts
				String query = "CONSTRUCT { ?concept ?pred1 ?label . " +
						"\n ?conceptURI ?pred2 ?obj2 ." +
						"\n ?obj2 ?pred3 ?label3 ." +
						"\n}" +
						"\nWHERE {" +
						"\n?concept ?pred ?xlabel ." +
						"\nFILTER( ?pred=<"+SKOSXLPREFLABEL+"> || " +
						"?pred = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"?pred = <"+SKOSXLALTLABEL+">)" +
						"\n?xlabel <"+SKOSXLLITERALFORM+"> ?label . "+
						
						"\n?concept <"+HASDATECREATED+"> ?created ."+
						"\nOPTIONAL{ ?concept <"+HASDATEUPDATED+"> ?updated .}"+
						"\nFILTER(" +
						"\n(!bound(?updated) " +
						"\n&& <"+DATETIME+">(?created) >"+startDate+
						"\n&& <"+DATETIME+">(?created) <"+endDate+" )" +
						"\n||" +
						"\n(bound(?updated) " +
						"\n&& <"+DATETIME+">(?updated) >"+startDate+
						"\n&& <"+DATETIME+">(?updated) <"+endDate+")" +
						"\n)" +
						
						"\nOPTIONAL { "+
						"\n ?conceptURI ?pred2 ?obj2 . "+
						"\nFILTER(?pred2 != <"+SKOSXLALTLABEL+"> && "+
						"\n?pred2 != <"+SKOSXLHIDDENLABEL+"> &&  "+
						"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
						"\n?pred2 != <"+LABEL+"> && "+
						"\n?pred2 != <"+PREFLABEL+"> && "+
						"\n?pred2 != <"+ALTLABEL+"> && "+
						"\n?pred2 != <"+HIDDENLABEL+"> && "+
						"\n?pred2 != <"+SEMANTICRELATION+">) . "+
						"\nOPTIONAL{ "+
						"\n	?obj2 a <"+CONCEPT+"> . "+
						"\n	?obj2 ?predTemp _:b3 . "+
						"\n	FILTER(?predTemp = <"+SKOSXLALTLABEL+"> || "+
						"\n?predTemp = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"\n?predTemp = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n	?obj2 ?pred3 _:b3 . "+
						"\n	_:b3 <"+SKOSXLLITERALFORM+"> ?label3 . "+
						"\n} "+
						"\n} "+

						"\n}";
				
				printQuery(query);
				
				GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
				ARTStatementIterator it = graphQuery.evaluate(true);
				
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
				skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
				result = out.toString();
				it.close();
				out.close();
				writer.close();
			} else 
				return "not implemented for format: "+format;
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getLatestUpdates"; 
		} catch (UnsupportedQueryLanguageException e1) {
			e1.printStackTrace();
			return errorMessage+"getLatestUpdates"; 
		} catch (MalformedQueryException e1) {
			e1.printStackTrace();
			return errorMessage+"getLatestUpdates"; 
		} catch (QueryEvaluationException e1) {
			e1.printStackTrace();
			return errorMessage+"getLatestUpdates"; 
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
			return errorMessage+"getLatestUpdates"; 
		} catch (IOException e) {
			e.printStackTrace();
			return errorMessage+"getLatestUpdates"; 
		}
		
		return result;
	}

	public String getAuthoritySegment(String ontologyName, int integerFrom, int integerTo, String format) {
		return DEPRECATED;
	}

	public String suggestTerm(String ontologyName, String label, String lang, String namespace,
			String parentURI) {
		return DEPRECATED;
	}

	// TODO DONE
	/**
	 * The Method getConceptByRelationshipValue will return the list of URI in the given OntologyName , 
	 * RelationURI, value and lang. The result of this method will be in TXT format.
	 * @param ontologyName
	 * @param relationURI
	 * @param value
	 * @param searchMode
	 * @param lang
	 * @param format
	 * @return the list of URI in the given OntologyName , RelationURI, value and lang
	 */
	public String getConceptByRelationshipValue(String ontologyName, String relationURI, String value,
			String searchMode, String lang, String format) {
//		logger.debug("getConceptByRelationshipValue:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"relationURI="+relationURI+
//				debugSeparator+"value="+value+
//				debugSeparator+"searchMode="+searchMode+
//				debugSeparator+"lang="+lang+
//				debugSeparator+"format="+format);
		String result = "";
		
		try{
			if(format.compareToIgnoreCase("txt") == 0){
				String query;
				
				query = "SELECT ?conceptURI ?label"+ 
						"\nWHERE { " +
						"\n{";
				
				query += "\n?conceptURI <"+relationURI+"> ?valueOfProp ." +
						"\nFILTER isLiteral(?valueOfProp)";
				if(searchMode.toLowerCase().contains("start")){
					query += "\nFILTER regex(str(?valueOfProp), \"^"+value+".*\" , \"s\")";
				} else if(searchMode.toLowerCase().contains("end")){
					query += "\nFILTER regex(str(?valueOfProp), \".*"+value+"$\", \"s\")";
				}else { // exact match
					query += "\nFILTER regex(str(?valueOfProp), \"^"+value+"$\")";
				}
				if(lang.compareToIgnoreCase("all") != 0){
					query += "\nFILTER langMatches(lang(?valueOfProp), \""+lang+"\")";
				}
				query+= "\n?conceptURI ?pred1 _:b1 . "+
						"\n FILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
						"?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"?pred1 = <"+SKOSXLPREFLABEL+"> ) ."+
						"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label .";
				
				query+="\n}" +
						"\nUNION" +
						"\n{";
				
				query += "?conceptURI <"+relationURI+"> ?valueOfProp ." +
						"\nFILTER isIRI(?valueOfProp)";
				if(searchMode.toLowerCase().contains("start")){
					query += "\nFILTER regex(str(?valueOfProp), \"^"+value+".*\" , \"s\")";
				} else if(searchMode.toLowerCase().contains("end")){
					query += "\nFILTER regex(str(?valueOfProp), \".*"+value+"$\", \"s\")";
				}else { // exact match
					query += "\nFILTER regex(str(?valueOfProp), \"^"+value+"$\")";
				}
				query+= "\n?conceptURI ?pred1 _:b2 . "+
						"\n FILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
						"?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"?pred1 = <"+SKOSXLPREFLABEL+"> ) ."+
						"\n_:b2 <"+SKOSXLLITERALFORM+"> ?label .";
				
				query +="\n}"+
						"\n}" +
						"\nORDER BY ?conceptURI ";
				
				
				printQuery(query);
				
				TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
				TupleBindingsIterator it = tupleQuery.evaluate(true);
				
				String lastConcept = "";
				int cont=0;
				while(it.hasNext()){
					//resultValues: ?conceptURI ?label
					TupleBindings tuple = it.next();
					String concept = tuple.getBinding("conceptURI").getBoundValue().asURIResource().getLocalName();
					if(lastConcept.compareTo(concept) != 0){
						lastConcept = concept;
						if(result.length()!=0)
							result +="]";
						result +="[";
					}
					else 
						result +=", ";
					ARTLiteral literal = tuple.getBinding("label").getBoundValue().asLiteral();
					result += literal.getLabel()+" ("+literal.getLanguage()+")";
				}
				it.close();
				if(result.length()>0)
					result += "]";
			} else{ // format == skos
				//Prepare a complex query which construct a graph containing all the result of this method
				String query;
						
						query = "CONSTRUCT {?conceptURI ?pred1 ?label1 . " +
								"\n?conceptURI ?pred2 ?obj2 ." +
								"\n?conceptURI ?pred3 ?obj3 ." +
								"\n?obj3 ?pred31 ?label3 ." +
								"\n?conceptURI <"+NARROWER+"> ?subjBroader ." +
								"\n?subjBroader ?pred41 ?label4 }" +
								
								"\nWHERE { "+
								"\n{"+
								
								"\n?conceptURI <"+relationURI+"> ?valueOfProp ." +
								"\nFILTER isLiteral(?valueOfProp)";
								if(searchMode.toLowerCase().contains("start")){
									query += "\nFILTER regex(str(?valueOfProp), \"^"+value+".*\" , \"s\")";
								} else if(searchMode.toLowerCase().contains("end")){
									query += "\nFILTER regex(str(?valueOfProp), \".*"+value+"$\", \"s\")";
								}else { // exact match
									query += "\nFILTER regex(str(?valueOfProp), \"^"+value+"$\")";
								}
								if(lang.compareToIgnoreCase("all") != 0){
									query += "\nFILTER langMatches(lang(?valueOfProp), \""+lang+"\")";
								}
								
								query+="\n}" +
										"\nUNION" +
										"\n{";
								
								query +="\n?conceptURI <"+relationURI+"> ?valueOfProp ." +
										"\nFILTER isIRI(?valueOfProp)";
								if(searchMode.toLowerCase().contains("start")){
									query += "\nFILTER regex(str(?valueOfProp), \"^"+value+".*\" , \"s\")";
								} else if(searchMode.toLowerCase().contains("end")){
									query += "\nFILTER regex(str(?valueOfProp), \".*"+value+"&\", \"s\")";
								}else { // exact match
									query += "\nFILTER regex(str(?valueOfProp), \"^"+value+"&\")";
								}
								
								query+= "\n}" +
								"\n{ "+
								"\n?conceptURI ?pred1 _:b1 . "+
								"\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
								"\n?pred1 = <"+SKOSXLPREFLABEL+"> ) . "+
								"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label1 . "+
								"\n} "+
								
								"\nUNION { "+
								"\n?conceptURI ?pred2 ?obj2 . "+
								"\nFILTER(?pred2 = <"+HASCOPENOTE+"> || "+
								"\n?pred2 = <"+HASEDITORIALNOTE+"> || "+
								"\n?pred2 = <"+HASDATEUPDATED+">) . "+
								"\n}"+
								
								"\nUNION { "+
								"\n?conceptURI ?pred3 ?obj3 . "+
								"\n	?obj3 a <"+CONCEPT+"> . "+
								"\n	FILTER(?pred3 = <"+BROADER+"> || "+
								"\n?pred3 = <"+RELATED+"> ) . "+
								"\n?obj3 ?pred31 _:b3"+
								"\n	FILTER(?pred31 = <"+SKOSXLALTLABEL+"> || "+
								"\n?pred31 = <"+SKOSXLHIDDENLABEL+"> ||  "+
								"\n?pred31 = <"+SKOSXLPREFLABEL+"> ) . "+
								"\n	_:b3 <"+SKOSXLLITERALFORM+"> ?label3 . "+
								"\n} "+
								
								//Since NARROWER is not normally used, it is better to use 
								// ?c2 BROADER ?c1
								//which is equivalent (but present) to
								// ?c1 NARROWER ?c2
								// so the results will have (left side in the WHERE and right side in the CONSCTRUCT)
								// ?c2 BROADER ?c1  ->  ?c1 NARROWER ?c2
								// ?c1 BROADER ?c3  ->  ?c1 BROADER ?c3
								"\nUNION { "+
								"\n?subjBroader <"+BROADER+"> ?conceptURI . "+
								"\n?subjBroader a <"+CONCEPT+"> . "+
								"\n?subjBroader ?pred41 _:b4"+
								"\n	FILTER(?pred41 = <"+SKOSXLALTLABEL+"> || "+
								"\n?pred41 = <"+SKOSXLHIDDENLABEL+"> ||  "+
								"\n?pred41 = <"+SKOSXLPREFLABEL+"> ) . "+
								"\n	_:b4 <"+SKOSXLLITERALFORM+"> ?label4 . "+
								"\n} "+

								"\n}"+
								"\nORDER BY ?conceptURI ";
				GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
				ARTStatementIterator it = graphQuery.evaluate(true);
				
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
				skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
				result = out.toString();
				it.close();
				out.close();
				writer.close();
			}
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByRelationshipValue";
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByRelationshipValue";
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByRelationshipValue";
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByRelationshipValue";
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByRelationshipValue";
		} catch (IOException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByRelationshipValue";
		}
		
		return result;
	}

	// TODO DONE
	/**
	 * The Method will return concepts with given keyword, and can filter with the language. 
	 * The result of this method will be in TXT or SKOS format.
	 * @param ontologyName
	 * @param conceptURI
	 * @param format TXT or SKOS
	 * @return concepts with given keyword, and can filter with the language
	 */
	public String getConceptByURI(String ontologyName, String conceptURI, String format) {
//		logger.debug("getConceptByURI:"+
//				debugSeparator+"ontologyName="+ontologyName+
//				debugSeparator+"conceptURI="+conceptURI+
//				debugSeparator+"format="+format);
		String result = "";

		//In this method, it is returning all the properties and their values for given concept and if the 
		// value is Concept, then also include all the labels.
		
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
			if(format.compareToIgnoreCase("skos") == 0) {
				//Prepare a complex query which construct a graph containing all the result of this method
				String query = "CONSTRUCT {<"+conceptURI+"> ?pred1 ?label . " +
						"\n<"+conceptURI+"> ?pred2 ?obj2 ." +
						"\n ?obj2 ?pred3 ?label3 }"+
						"\nWHERE { "+
						"\n{ "+
						"\n<"+conceptURI+"> ?pred1 _:b1 . "+
						"\nFILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
						"\n?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"\n?pred1 = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label . "+
						"\n} "+
						"\nUNION { "+
						"\n<"+conceptURI+"> ?pred2 ?obj2 . "+
						"\nFILTER(?pred2 != <"+SKOSXLALTLABEL+"> && "+
						"\n?pred2 = <"+SKOSXLHIDDENLABEL+"> &&  "+
						"\n?pred2 != <"+SKOSXLPREFLABEL+"> && "+
						"\n?pred2 != <"+LABEL+"> && "+
						"\n?pred2 != <"+PREFLABEL+"> && "+
						"\n?pred2 != <"+ALTLABEL+"> && "+
						"\n?pred2 != <"+HIDDENLABEL+"> && "+
						"\n?pred2 != <"+SEMANTICRELATION+">) . "+
						"\nOPTIONAL{ "+
						"\n	?obj2 a <"+CONCEPT+"> . "+
						"\n	?obj2 ?predTemp _:b3 . "+
						"\n	FILTER(?predTemp = <"+SKOSXLALTLABEL+"> || "+
						"\n?predTemp = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"\n?predTemp = <"+SKOSXLPREFLABEL+"> ) . "+
						"\n	?obj2 ?pred3 _:b3 . "+
						"\n	_:b3 <"+SKOSXLLITERALFORM+"> ?label3 . "+
						"\n} "+
						"\n} "+
						"\n}";
				printQuery(query);
				GraphQuery graphQuery = skosxlModel.createGraphQuery(QueryLanguage.SPARQL, query);
				ARTStatementIterator it = graphQuery.evaluate(true);
				
				skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
				result = out.toString();
				it.close();
				out.close();
				writer.close();
			} else{ // format == "txt"
				//prepare a query which extract only the label of the selected concept
				String query = "SELECT ?label "+ 
						"\nWHERE { "+
						"\n<"+conceptURI+"> ?pred1 _:b1 . "+
						"\n FILTER(?pred1 = <"+SKOSXLALTLABEL+"> || "+
						"?pred1 = <"+SKOSXLHIDDENLABEL+"> ||  "+
						"?pred1 = <"+SKOSXLPREFLABEL+"> ) ."+
						"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label ."+
						"}";
				printQuery(query);
				TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
				TupleBindingsIterator it = tupleQuery.evaluate(true);
				result = "[";
				while(it.hasNext()){
					if(result.length()>1)
						result +=",";
					TupleBindings tuple = it.next();
					ARTLiteral literal = tuple.iterator().next().getBoundValue().asLiteral();
					result += literal.getLabel()+" ("+literal.getLanguage()+")";
				}
				it.close();
				result +="]";
			}

		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByURI";
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByURI";
		} catch (IOException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByURI";
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByURI";
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByURI";
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getConceptByURI";
		}

		return result;
	}

	public String getRejectedConcepts(String authorityFile, String dateFrom, String dateTo, int integerFrom,
			int integerTo, String format) {
		return DEPRECATED;
	}

	public String getTermCodeByTerm(String term) {
		return DEPRECATED;
	}

	public String getTermCodeByTermXML(String term, String format) {
		return DEPRECATED;
	}

	// TODO DONE
	/**
	 * Returns the label of a term specified by its AGROVOC internal termcode and the language code. 
	 * Can specify more than one language code using comma separator
	 * @param termcode
	 * @param language
	 * @return the label of a term specified by its AGROVOC internal termcode and the language code
	 */
	public String getTermByLanguage(int termcode, String language) {
//		logger.debug("getTermByLanguage:"+
//				debugSeparator+"termcode="+termcode+
//				debugSeparator+"language="+language);
		String result = "";
		String [] langArray = language.split(",");
		// prepare a SPARQL QUERY to obtain the label from the term code
		try {
			String query = "SELECT DISTINCT ?label "+
				"\nWHERE  {" +
				//"\n?xlabel <"+ HASCODEAGROVOC +"> \""+termcode +"\"^^<"+STRINGRDF+"> ."+ // old
				"\n?xlabelInitial <"+ NOTATION +"> \""+termcode +"\"^^<"+AGROVOCCODE+"> ."+
				"\n?concept ?predForXLabel1 ?xlabelInitial . "+
				"\nFILTER(?predForXLabel1 = <"+SKOSXLPREFLABEL+"> || " +
						"?predForXLabel1 = <"+SKOSXLALTLABEL+"> || " +
						"?predForXLabel1 = <"+SKOSXLHIDDENLABEL+"> ) " +
				
				//if just the label associated to the term with the specified concept should be used,
				// comment the following lines
				/*"\n?concept ?predForXLabel2 ?xlabelToUse . "+
				"\nFILTER(?predForXLabel2 = <"+SKOSXLPREFLABEL+"> || " +
						"?predForXLabel2 = <"+SKOSXLALTLABEL+"> || " +
						"?predForXLabel2 = <"+SKOSXLHIDDENLABEL+"> ) " +*/
				
						
				//Choose between taking all the labels associated to a concept (first line) or 
				//just the labels associatd to the terms with the specified termcode (second line)
				//"\n?xlabelToUse <"+ SKOSXLLITERALFORM +"> ?label" +" .";
				"\n?xlabelInitial <"+ SKOSXLLITERALFORM +"> ?label" +" .";
			
			if(language.compareTo("") == 0)
				return "";
			
			query +="\nFILTER(";
			for(int i=0; i<langArray.length; ++i){
				query += "langMatches(lang(?label), \""+langArray[i]+"\")";
				if(i+1<langArray.length)
					query+=" || ";
			}
			
			query +=")\n}";
			printQuery(query);
			TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
			TupleBindingsIterator it = tupleQuery.evaluate(true);
			

			result = "";
			while(it.hasNext()){
				TupleBindings tuple = it.next();
				Iterator<Binding> iterInTuple = tuple.iterator();
				ARTLiteral label = iterInTuple.next().getBoundValue().asLiteral();
				result += label.getLabel()+",";
			}
			it.close();
			if(result.length()>1)
				result = result.substring(0, result.length()-1);
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getTermByLanguage"; 
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getTermByLanguage"; 
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getTermByLanguage"; 
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getTermByLanguage"; 
		} 
		
		return result;
	}

	// TODO DONE
	/**
	 * Returns labels in all available languages of an AGROVOC term specified by its termcode. 
	 * @param termcode
	 * @param separator
	 * @return labels in all available languages of an AGROVOC term specified by its termcode
	 */
	public String getAllLabelsByTermcode2(int termcode, String separator) {
//		logger.debug("getAllLabelsByTermcode2:"+
//				debugSeparator+"termcode="+termcode+
//				debugSeparator+"separator="+separator);
		String result = "";
		if (separator == null || separator.compareTo("") == 0)
			separator = "||";
		// prepare a SPARQL QUERY to obtain the label from the term code
		try {
			String query = "SELECT DISTINCT ?label "+
				"\nWHERE  {" +
				//"\n?xlabel <"+ HASCODEAGROVOC +"> \""+termcode +"\"^^<"+STRINGRDF+"> ."+ // old
				/*"\n?xlabel <"+ NOTATION +"> \""+termcode +"\"^^<"+AGROVOCCODE+"> ."+
				"\n?xlabel <"+ SKOSXLLITERALFORM +"> ?label" +" ."+*/ // old
				
				"\n?xlabelInitial <"+ NOTATION +"> \""+termcode +"\"^^<"+AGROVOCCODE+"> ."+
				"\n?concept ?predForXLabel1 ?xlabelInitial . "+
				"\nFILTER(?predForXLabel1 = <"+SKOSXLPREFLABEL+"> || " +
						"?predForXLabel1 = <"+SKOSXLALTLABEL+"> || " +
						"?predForXLabel1 = <"+SKOSXLHIDDENLABEL+"> ) " +
				
				//if just the label associated to the term with the specified concept should be used,
				// comment the following lines
				/*"\n?concept ?predForXLabel2 ?xlabelToUse . "+
				"\nFILTER(?predForXLabel2 = <"+SKOSXLPREFLABEL+"> || " +
						"?predForXLabel2 = <"+SKOSXLALTLABEL+"> || " +
						"?predForXLabel2 = <"+SKOSXLHIDDENLABEL+"> ) " +*/
				
				
				//Choose between taking all the labels associated to a concept (first line) or 
				//just the labels associatd to the terms with the specified termcode (second line)
				//"\n?xlabelToUse <"+ SKOSXLLITERALFORM +"> ?label" +" ."+
				"\n?xlabelInitial <"+ SKOSXLLITERALFORM +"> ?label" +" ."+
				
				"\n}";
			printQuery(query);
			TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
			TupleBindingsIterator it = tupleQuery.evaluate(true);
			

			result = "[";
			while(it.hasNext()){
				TupleBindings tuple = it.next();
				Iterator<Binding> iterInTuple = tuple.iterator();
				ARTLiteral label = iterInTuple.next().getBoundValue().asLiteral();
				result += label.getLabel()+separator+label.getLanguage()+separator;
			}
			it.close();
			if(result.length()>1)
				result = result.substring(0, result.length()-separator.length());
			result +="]";
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getAllLabelsByTermcode2"; 
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getAllLabelsByTermcode2"; 
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getAllLabelsByTermcode2"; 
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getAllLabelsByTermcode2"; 
		} 
		
		return result;
	}

	// TODO DONE
	/**
	 * Description: Returns all AGROVOC terms containing the specified search string in the specified 
	 * search mode.
	 * @param searchString
	 * @param searchmode
	 * @param separator
	 * @return all AGROVOC terms containing the specified search string in the specified 
	 * search mode
	 */
	public String simpleSearchByMode2(String searchString, String searchmode, String separator) {
		logger.debug("simpleSearchByMode2:"+
				debugSeparator+"searchString="+searchString+
				debugSeparator+"searchmode="+searchmode+
				debugSeparator+"separator="+separator);
		String result="";
		//Collection<ARTURIResource> retrievedResources = null;
		Collection<ARTLiteral> retrievedResources = null;
		
		int cont = 0;
		String query = "";
		if(separator == null || separator.length()==0)
			separator = "||";
		try {
			boolean withIndexes = true;

			
			// remove the folliwing comment (and maybe change the boolean check to not use the indexes)
			//if(separator.length() >2) 
			//	withIndexes = false;
			
			if(withIndexes){
				retrievedResources = searchLabelsCaseInsensitive(searchString, searchmode, "all");
				
				if(retrievedResources == null)
					return DEPRECATED+" search mode: "+searchmode;
//				logger.debug("searchLabelsCaseInsensitive: number of results from search = "+retrievedResources.size());
				
				if(retrievedResources.size() == 0){
					result = "[";
				} else {
					
					// Create a SPARQL query with all the retrieve result
					query = "SELECT ?code ?label "+ 
							"\nWHERE { ";
					Iterator<ARTLiteral> iter = retrievedResources.iterator();
									
					query +="\nFILTER(";
					boolean first = true;
					while(iter.hasNext()){
						if(!first)
							query += " || ";
						first = false;
						ARTLiteral artLiteral = iter.next().asLiteral();
						query += "\n?label = \""+artLiteral.getLabel()+"\"@"+artLiteral.getLanguage();
					}
					query+=")";
					
					if(searchmode.toLowerCase().contains("word")){ // exact word
						query += "\nFILTER regex(str(?label), \""+searchString+"\" , \"i\")";
					} else if(searchmode.toLowerCase().contains("exact")){ // exact match
						query += "\nFILTER regex(str(?label), \"^"+searchString+"$\" , \"i\")";
					} else if(searchmode.toLowerCase().contains("start")){
						query += "\nFILTER regex(str(?label), \"^"+searchString+".*\" , \"i\")";
					} else if(searchmode.toLowerCase().contains("contain")){
						query += "\nFILTER regex(str(?label), \".*"+searchString+".*\" , \"i\")";
					} else if(searchmode.toLowerCase().contains("end")){
						query += "\nFILTER regex(str(?label), \".*"+searchString+"$\", \"i\")";
					}
					
					query +="\n{ ?conceptURI <"+SKOSXLPREFLABEL+"> ?xlabel . }"+
							"\nUNION"+
							"\n{ ?conceptURI <"+SKOSXLALTLABEL+"> ?xlabel . }"+
							"\nUNION"+
							"\n{ ?conceptURI <"+SKOSXLHIDDENLABEL+"> ?xlabel . }";
							
					
					query +="\n?xlabel <"+SKOSXLLITERALFORM+"> ?label ." +
							//"\n?xlabel <"+HASCODEAGROVOC+"> ?code."+ // old
							"\n?xlabel <"+NOTATION+"> ?code."+
							"\n}"+
							"\nORDER BY ?code ";
					printQuery(query);
					TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, 
							query);
					TupleBindingsIterator it = tupleQuery.evaluate(true);
					result = "[";
					while(it.hasNext()){
						++cont;
						TupleBindings tuple = it.next();
						String code = tuple.getBoundValue("code").asLiteral().getLabel();
						ARTLiteral literal = tuple.getBoundValue("label").asLiteral();
						result += code+separator+literal.getLabel()+separator+literal.getLanguage()+separator;
					}
					it.close();
				}
			}
			else{
			
				//NO INDEXES
				query = "SELECT ?code ?label "+ 
						"\nWHERE { "+
						"\n_:b1 <"+SKOSXLLITERALFORM+"> ?label .";
				
				if(searchmode.toLowerCase().contains("exact")){
					query += "\nFILTER regex(str(?label), \"^"+searchString+"$\" , \"i\")";
				} else if(searchmode.toLowerCase().contains("start")){
					query += "\nFILTER regex(str(?label), \"^"+searchString+".*\" , \"i\")";
				} else if(searchmode.toLowerCase().contains("contain")){
					query += "\nFILTER regex(str(?label), \".*"+searchString+".*\" , \"i\")";
				} else if(searchmode.toLowerCase().contains("end")){
					query += "\nFILTER regex(str(?label), \".*"+searchString+"$\", \"i\")";
				}
				
				query +=//"\n_:b1 <"+HASCODEAGROVOC+"> ?code ."+ // old
						"\n_:b1 <"+NOTATION+"> ?code ."+
						"\n}";
				printQuery(query);
				
				TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
				TupleBindingsIterator it = tupleQuery.evaluate(true);
				result = "[";
				while(it.hasNext()){
					++cont;
					TupleBindings tuple = it.next();
					String code = tuple.getBoundValue("code").asLiteral().getLabel();
					ARTLiteral literal = tuple.getBoundValue("label").asLiteral();
					result += code+separator+literal.getLabel()+separator+literal.getLanguage()+separator;
				}
				it.close();
			}
			
			result +="NumberOfResults"+separator+cont+"]";
		} catch (IndexAccessException e) {
			e.printStackTrace();
			return errorMessage+"simpleSearchByMode2"; 
		} catch (ParseException e) {
			e.printStackTrace();
			return errorMessage+"simpleSearchByMode2"; 
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"simpleSearchByMode2"; 
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"simpleSearchByMode2"; 
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"simpleSearchByMode2"; 
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"simpleSearchByMode2"; 
		}
		return result;
	}

	// TODO DONE
	/**
	 * Returns the most important concept information of an AGROVOC term, i.e. its term code, labels in all 
	 * languages and the term codes of its broader, narrower and related terms as well as of non-descriptor 
	 * terms for which this concept is used. In case of a non-descriptor, it contains the link to its 
	 * descriptor via the USE relationship. So compared to its previous version of this method 
	 * (getConceptByTerm) it is now possible to navigate from and to non-descriptors. Also, now the 
	 * termcode is used instead of the term, to avoid ambiguity (in case of two identical terms in 
	 * different languages).
	 * @param termCode
	 * @return
	 */
	public String[] getConceptInfoByTermcode(String termCode) {
//		logger.debug("getConceptInfoByTermcode:"+
//				debugSeparator+"termCode="+termCode);
		String []result = new String [7];
		// prepare a SPARQL QUERY to obtain all the relevant information from the term code
		try {
			
			//to speed up the process prepare two query, the first one retrieve the concept
			//( in theory just one concept, but in few cases two concepts could be retrieves)
			
			//first query
			String query = "SELECT DISTINCT ?concept ?labelType" +
					"\nWHERE{" +
					"\n?xlabel <"+ NOTATION +"> \""+termCode +"\"^^<"+AGROVOCCODE+"> ."+
					"\n?concept ?labelType ?xlabel . "+
					"\nFILTER(?labelType = <"+SKOSXLPREFLABEL+"> || " +
					"?labelType = <"+SKOSXLALTLABEL+"> || " +
					"?labelType = <"+SKOSXLHIDDENLABEL+"> ) " +
					"\n}";
			printQuery(query);
			TupleQuery tupleQuery = skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
			TupleBindingsIterator it = tupleQuery.evaluate(true);
			List<String> conceptList = new ArrayList<String>();
			String labelType = "";
			while(it.hasNext()){
				TupleBindings tuple = it.next();
				conceptList.add(tuple.getBinding("concept").getBoundValue().getNominalValue());
				labelType = tuple.getBinding("labelType").getBoundValue().getNominalValue();
			}
			it.close();
			
			//use maps and lists to avoid duplicates
			List<String> narrowerList = new ArrayList<String>();
			List<String> broaderList = new ArrayList<String>();
			List<String> relatedList = new ArrayList<String>();
			List<String> USEorUFList = new ArrayList<String>();
			Map <String, ARTLiteral> labelsMap = new HashMap<String, ARTLiteral>();
			
			//second query
			if(conceptList.size()>0){
				query = "SELECT ?label ?termCodeUSEorUF ?genericProperty ?otherTerm ?broaderProperty ?broaderTerm"+
					"\nWHERE  {";
					
					boolean first = true;
					
					
					query+="\n?xlabelToUse <"+ NOTATION +"> \""+termCode +"\"^^<"+AGROVOCCODE+"> ."+
					
					"\n{" +
					"\n?xlabelToUse <"+ SKOSXLLITERALFORM +"> ?label" +" . " +
					"\n}"+
					
					
					"\nUNION{"+
					"\n?xlabelToUse ?p1 ?xlabel1 ."+
					"\n?xlabel1 a <"+LABEL_CLASS +"> ."+
					"\n?xlabel1 <"+ NOTATION +"> ?termCodeUSEorUF ."+ 
					"\n}"+
					
					"\nUNION{";
					//"\n?concept ?genericProperty ?otherConcept . " +
					first = true;
					for(String concept : conceptList){
						if(!first)
							query+="\nUNION";
						query+="\n{<"+concept+"> ?genericProperty ?otherConcept . }";
						first = false;
					}
					query+="\n?otherConcept ?labelTypeTemp ?otherXLabel ."+
					"\nFILTER(?labelTypeTemp = <"+SKOSXLPREFLABEL+"> || " +
					"?labelTypeTemp = <"+SKOSXLHIDDENLABEL+"> ||  "+
					"?labelTypeTemp = <"+SKOSXLALTLABEL+"> )" +
					//"\n?otherConcept ?labelType ?otherXLabel ."+
					"\n?otherXLabel <"+NOTATION+"> ?otherTerm ." +
					"\n}"+
					
					//Since NARROWER is not normally used, it is better to use 
					// ?c2 BROADER ?c1
					//which is equivalent (but present) to
					// ?c1 NARROWER ?c2
					// so the results will have (left side in the WHERE and right side in the CONSCTRUCT)
					// ?c2 BROADER ?c1  ->  ?c1 NARROWER ?c2
					// ?c1 BROADER ?c3  ->  ?c1 BROADER ?c3
					"\nUNION {";
					//"\n?concept ?genericProperty ?otherConcept . " +
					first = true;
					for(String concept : conceptList){
						if(!first)
							query+="\nUNION";
						query+="\n{?subjBroader <"+BROADER+"> <"+concept+"> ." +
								"\n?subjBroader ?broaderProperty <"+concept+"> }";
						first = false;
					}
					query+="\n?subjBroader ?labelTypeTempBroader ?broaderXLabel ."+
					"\nFILTER(?labelTypeTempBroader = <"+SKOSXLPREFLABEL+"> || " +
					"?labelTypeTempBroader = <"+SKOSXLHIDDENLABEL+"> ||  "+
					"?labelTypeTempBroader = <"+SKOSXLALTLABEL+"> )" +
					//"\n?subjBroader ?labelTypeBroader ?otherXLabel ."+
					"\n?broaderXLabel <"+NOTATION+"> ?broaderTerm ." +
					"\n}"+
					
					
					"\n}";
				printQuery(query);
				tupleQuery = skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
				it = tupleQuery.evaluate(true);
				
				
				
				while(it.hasNext()){
					TupleBindings tuple = it.next();
					//return values: ?labelType ?label ?termCodeUSEorUF ?narrowerTerm ?broaderTerm ?relatedTerm
					if(tuple.hasBinding("label")){
						ARTLiteral label = tuple.getBinding("label").getBoundValue().asLiteral();
						if(!labelsMap.containsKey(label.getLabel()))
							labelsMap.put(label.getLabel(), label);
					}
					if(tuple.hasBinding("termCodeUSEorUF")){
						String termCodeUSEorUF = tuple.getBinding("termCodeUSEorUF").getBoundValue().getNominalValue();
						if(!USEorUFList.contains(termCodeUSEorUF))
							USEorUFList.add(termCodeUSEorUF);
					}
					if(tuple.hasBinding("genericProperty")){
						String genericProperty = tuple.getBinding("genericProperty").getBoundValue().getNominalValue();
						if(genericProperty.compareTo(NARROWER) == 0) {
							//Do nothing the narrower concepts are dealt using the broaderProperty value
							
							/*String narrowC = tuple.getBinding("otherTerm").getBoundValue().getNominalValue();
							if(!narrowerList.contains(narrowC))
								narrowerList.add(narrowC);*/
						}
						else if(genericProperty.compareTo(BROADER) == 0) {
							String broaderC = tuple.getBinding("otherTerm").getBoundValue().getNominalValue();
							if(!broaderList.contains(broaderC))
								broaderList.add(broaderC);
						}
						else{ // the property is not NARROWER nor BROADER, but it goes in the related, because 
							// NARROWER and BROADER are subproperties of semanticRelation, so when constructing 
							// the asnwer they should not be included
							String relatedC = tuple.getBinding("otherTerm").getBoundValue().getNominalValue();
							if(!relatedList.contains(relatedC))
								relatedList.add(relatedC);
						}
					}
					if(tuple.hasBinding("broaderProperty")){
						String narrowC = tuple.getBinding("broaderTerm").getBoundValue().getNominalValue();
						if(!narrowerList.contains(narrowC))
							narrowerList.add(narrowC);
					}
				}
				it.close();
			}
			
			//now create the response array of String
			int pos = 0;
			String tempResult = "";
			tempResult += "["+termCode+"]";
			result[pos++] = tempResult;
			tempResult = "[";
			boolean first = true;
			for(String key : labelsMap.keySet()){
				ARTLiteral literal = labelsMap.get(key);
				if(!first)
					tempResult += ",";
				first = false;
				tempResult += literal.getLabel()+","+literal.getLanguage().toUpperCase();
			}
			tempResult += "]";
			result[pos++] = tempResult;
			
			tempResult = "[UF";
			if(labelType.toLowerCase().contains("preflabel")){
				for(String uf : USEorUFList)
					tempResult += ", "+uf;
			}
			tempResult += "]";
			result[pos++] = tempResult;
			
			tempResult = "[USE";
			if(labelType.toLowerCase().contains("altlabel")){
				for(String uf : USEorUFList)
					tempResult += ", "+uf;
			}
			tempResult += "]";
			result[pos++] = tempResult;
			
			tempResult = "[BT";
			for(String bt : broaderList)
				tempResult += ", "+bt;
			tempResult += "]";
			result[pos++] = tempResult;
			
			tempResult="[NT";
			for(String nt : narrowerList)
				tempResult += ", "+nt;
			tempResult += "]";
			result[pos++] = tempResult;
			
			tempResult="[RT";
			for(String rt : relatedList){
				if(!narrowerList.contains(rt) && !broaderList.contains(rt))
					tempResult += ", "+rt;
			}
			tempResult += "]";
			result[pos++] = tempResult;
			
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	public String getConceptInfoByTermcodeXML(String searchTerm, String format) {
		return DEPRECATED;
	}

	public String complexSearchByTermXML(String searchString, String separator, String format) {
		return DEPRECATED;
	}

	// TODO DONE
	/**
	 * Returns the history notes, definitions, scope notes. 
	 * @param termcode
	 * @param languagecode
	 * @return the history notes, definitions, scope notes.
	 */
	public String[] getDefinitions(int termcode, String languagecode) {
//		logger.debug("getDefinitions:"+
//				debugSeparator+"termcode="+termcode+
//				debugSeparator+"languagecode="+languagecode);
		String []result = new String[4];
		// prepare a SPARQL QUERY to obtain all the relevant information from the term code
		try {
			String query = "SELECT ?scopeNote ?editorialNote ?definition ?codeFishery"+
				"\nWHERE  {" +
				//"\n?xlabel <"+ HASCODEAGROVOC +"> \""+termcode +"\"^^<"+STRINGRDF+"> ."+ // old
				/*"\n?xlabel <"+ NOTATION +"> \""+termcode +"\"^^<"+AGROVOCCODE+"> ."+
				"\n?xlabel <"+SKOSXLLITERALFORM+"> _:b1 . "+
				"\nFILTER(?labelType = <"+SKOSXLPREFLABEL+"> || " +
				"?labelType = <"+SKOSXLALTLABEL+"> )" +
				"\n?concept ?labelType ?xlabel ."+*/ // old
				
				"\n?xlabelInitial <"+ NOTATION +"> \""+termcode +"\"^^<"+AGROVOCCODE+"> ."+
				"\n?concept ?predForXLabel1 ?xlabelInitial . "+
				"\nFILTER(?predForXLabel1 = <"+SKOSXLPREFLABEL+"> || " +
						"?predForXLabel1 = <"+SKOSXLALTLABEL+"> || " +
						"?predForXLabel1 = <"+SKOSXLHIDDENLABEL+"> ) " +
						"\n?concept ?predForXLabel2 ?xlabelToUse . "+
				
				/*"\nFILTER(?predForXLabel2 = <"+SKOSXLPREFLABEL+"> || " +
						"?predForXLabel2 = <"+SKOSXLALTLABEL+"> || " +
						"?predForXLabel2 = <"+SKOSXLHIDDENLABEL+"> ) " +
				"\n?xlabelToUse <"+ SKOSXLLITERALFORM +"> ?label" +" ."+*/ //old and without any meaning
				
				"\nOPTIONAL{" +
				"\n?concept ?propScopeNote ?scopeNote ." + 
				"\nFILTER (?propScopeNote = <"+HASCOPENOTE+">)" +
				"\nFILTER(langMatches(lang(?scopeNote), \""+languagecode+"\" ))" +
				"\n}" +
				
				"\nOPTIONAL{" +
				"\n?concept ?propEditorialNote ?editorialNote ." + 
				"\nFILTER (?propEditorialNote = <"+HASEDITORIALNOTE+">)" +
				"\nFILTER(langMatches(lang(?propEditorialNote), \""+languagecode+"\" ))" +
				"\n}"+
				
				"\nOPTIONAL {" +
				"\n?concept <"+DEFINITION+"> ?definitionBNode ." +
				"\n?definitionBNode <"+RDFVALUE+"> ?definition ." +
				"\nFILTER(langMatches(lang(?definition), \""+languagecode+"\" ))" +
				"\n}"+
				
				"\nOPTIONAL {" +
				"\n?concept <"+HASCODEFISCHERY3ALPHA+"> ?fishery ." +
				"\nFILTER(langMatches(lang(?fishery), \""+languagecode+"\" ))" +
				"\n}" +
				"\n}";
				
			printQuery(query);
			TupleQuery tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
			TupleBindingsIterator it = tupleQuery.evaluate(true);
			
			//use maps and lists to avoid duplicates
			List<String> scopeList = new ArrayList<String>();
			List<String> editoriaList = new ArrayList<String>();
			List<String> definitionList = new ArrayList<String>();
			List<String> codeFishery3AlphaList = new ArrayList<String>();
			
			//boolean emptyResponse = true; 
			List<String> varNames = it.getBindingNames();
			while(it.hasNext()){
				TupleBindings tuple = it.next();
				//return values order: ?scopeNote ?editorialNote ?definition ?codeFishery
				
				for(String varName : varNames){
					if(!tuple.hasBinding(varName)){
						continue;
					}
					ARTNode artNode = tuple.getBoundValue(varName);
					
					String value;
					if(artNode.isLiteral())
						value = artNode.asLiteral().getLabel();
					else if(artNode.isURIResource())
						value = artNode.asURIResource().getURI();
					else
						continue;
						
					if(varName.compareTo("scopeNote") == 0 &&  !scopeList.contains(value))
						scopeList.add(value);
					else if(varName.compareTo("editorialNote") == 0 &&  !editoriaList.contains(value))
						editoriaList.add(value);
					else if(varName.compareTo("definition") == 0 &&  !definitionList.contains(value))
						definitionList.add(value);
					else if(varName.compareTo("codeFishery") == 0 &&  !codeFishery3AlphaList.contains(value))
						codeFishery3AlphaList.add(value);
				}
			}
			it.close();
			
			//now create the response string
			String tempResult;
			int pos = 0;
			tempResult= "[Scope Note:";
			for(String scope : scopeList)
				tempResult += scope+", ";
			if(tempResult.endsWith(", "))
				tempResult = tempResult.substring(0, tempResult.length()-2);
			tempResult+="]";
			result[pos++] = tempResult;
			
			tempResult = "[History Note:";
			for(String editorial : editoriaList)
				tempResult += editorial+", ";
			if(tempResult.endsWith(", "))
				tempResult = tempResult.substring(0, tempResult.length()-2);
			tempResult+="]";
			result[pos++] = tempResult;
			
			
			tempResult = "[Definition:";
			for(String definition : definitionList)
				tempResult += definition+", ";
			if(tempResult.endsWith(", "))
				tempResult = tempResult.substring(0, tempResult.length()-2);
			tempResult+="]";
			result[pos++] = tempResult;
			
			tempResult = "[Comment:";
			for(String codeFishery : codeFishery3AlphaList)
				tempResult += codeFishery+", ";
			if(tempResult.endsWith(", "))
				tempResult = "Fishery 3 alpha code:"+tempResult.substring(0, tempResult.length()-2);
			tempResult+="]";
			result[pos++] = tempResult;
			
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			//return errorMessage+"getDefinitions"; 
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			//return errorMessage+"getDefinitions"; 
		} catch (ModelAccessException e) {
			e.printStackTrace();
			//return errorMessage+"getDefinitions"; 
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			//return errorMessage+"getDefinitions"; 
		} 
		
		return result;
	}

	public void close() {
		try {
			skosxlModel.close();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		}
	}
	
	
	// TODO DONE
	/**
	 * Returns the release date of the dataset
	 */

	public String getReleaseDate(){
//		logger.debug("getReleaseDate:");
		String releaseDate = null;
		
		String query = "SELECT ?date" +
				"\nWHERE{" +
				"\n<"+AGROVOC +"> <"+HASDATEUPDATED+"> "+" ?date ."+
				"\n}";
		printQuery(query);
		
		TupleQuery tupleQuery;
		try {
			tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);
			TupleBindingsIterator it = tupleQuery.evaluate(true);
			if(it.hasNext()){
				TupleBindings tuple = it.next();
				Iterator<Binding> iterInTuple = tuple.iterator();
				releaseDate = iterInTuple.next().getBoundValue().asLiteral().getNominalValue().split("T")[0];
			} else{
				// Agrovoc has no modified date
				releaseDate = "no release date";
			}
			it.close();
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
			return errorMessage+"getReleaseDate";
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return errorMessage+"getReleaseDate";
		} catch (MalformedQueryException e) {
			e.printStackTrace();
			return errorMessage+"getReleaseDate";
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
			return errorMessage+"getReleaseDate";
		}
		
		return releaseDate;
	}
	
	// TODO DONE
	/**
	 * Returns the version of the deployed web services
	 */

	public String getWebServicesVersion(){
//		logger.debug("getWebServicesVersion:");
		String version = null;
		
		String filePath = null;
		
		 File file = new File(fileNameVersion); 
		 // useful only if launched and the property file is in the same directory
         // for example when launched inside Eclipse
         if (!file.exists()) { // search the property file recursive in all the directory
                File propFile = searchFile("..", fileNameVersion);
                if(propFile!=null) {
                	filePath =  propFile.getAbsolutePath();
                }
         } else
        	 filePath = fileNameVersion;
         // no property file was found
		
		Properties configVersionFile = new Properties();
		InputStream is;
		try {
			is = new FileInputStream(filePath);
			configVersionFile.load(is);

		} catch (FileNotFoundException e) {
			return "noVersion";
		} catch (IOException e) {
			return "noVersion";
		}
		
		version = configVersionFile.getProperty("version");

		return version;
	}
	
	
	/*******************************************************
	 * 
	 * PRIVATE METHODS
	 * 
	 *******************************************************/
	private String getConceptLabelsByURI(String ontologyName, String conceptURI, String format) {
		String result = "";

		// initialize();

		ARTURIResource concept = skosxlModel.createURIResource(conceptURI);
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Writer writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
			// RDFIterator<ARTStatement> it = new PrefLabelsRDFIterator(concept);
			skosxlModel.listPrefLabels(concept, true, NodeFilters.ANY);
			RDFIterator<ARTStatement> it = new AllLabelsRDFIterator(concept);
			// skosxlModel.writeRDF(it, RDFFormat.RDFXML, out);
			// skosxlModel.writeRDF(it, RDFFormat.RDFXML, writer);
			skosxlModel.writeRDF(it, RDFFormat.RDFXML_ABBREV, writer);
			writer.close();
			result = out.toString();

		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}
	
	private Collection<ARTURIResource> searchConcept(String term, String searchMode, String lang ) 
			throws IndexAccessException, ParseException{
		
		if(useOWLIMIndexes)
			return searchConceptWithOWLIMIndexes(term, searchMode, lang);
			
		//else do not use the OWLIM indexes
		
		
		Collection<ARTURIResource> retrievedResources = null;
		if(lang.compareToIgnoreCase("all") == 0){
			//search with all the possible languages
			retrievedResources = new ArrayList<ARTURIResource>();
			for(int i=0; i<languages.length; ++i) {
				Collection<ARTURIResource> tempRetrievedResources = null;
				// the possible searchMode are: contains, exact match, starts with, ends with
				//if (searchMode.compareToIgnoreCase("exact match") == 0) {
				if (searchMode.toLowerCase().contains("exact") ) {
					QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.EXACT);
					String processedQueryString = qc.compile(term);
					tempRetrievedResources = skosSearchEngine.findConcepts(processedQueryString, languages[i], 
							OntIndexSearcher.EXACT_MATCH);
				} else if (searchMode.toLowerCase().contains("contain")) {
					QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.CONTAINS);
					String processedQueryString = qc.compile(term);
					tempRetrievedResources = skosSearchEngine.findConcepts(processedQueryString, languages[i],
							OntIndexSearcher.EXACT_MATCH);
				} else if (searchMode.toLowerCase().contains("start")) {
					QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.STARTSWITH);
					String processedQueryString = qc.compile(term);
					tempRetrievedResources = skosSearchEngine.findConcepts(processedQueryString, languages[i],
							OntIndexSearcher.EXACT_MATCH);
				} else if (searchMode.toLowerCase().contains("end")) {
					QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.ENDSWITH);
					String processedQueryString = qc.compile(term);
					tempRetrievedResources = skosSearchEngine.findConcepts(processedQueryString, languages[i],
							OntIndexSearcher.EXACT_MATCH);
				} else {
					return null;
				}
				retrievedResources.addAll(tempRetrievedResources);
			}
		} else{
			// the possible searchMode are (searching for these strings): contain, exact, start, end
			if (searchMode.toLowerCase().contains("exact") ) {
				QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.EXACT);
				String processedQueryString = qc.compile(term);
				retrievedResources = skosSearchEngine.findConcepts(processedQueryString, lang, 
						OntIndexSearcher.EXACT_MATCH);
			} else if (searchMode.toLowerCase().contains("contain")) {
				QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.CONTAINS);
				String processedQueryString = qc.compile(term);
				retrievedResources = skosSearchEngine.findConcepts(processedQueryString, lang,
						OntIndexSearcher.EXACT_MATCH);
			} else if (searchMode.toLowerCase().contains("start")) {
				QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.STARTSWITH);
				String processedQueryString = qc.compile(term);
				retrievedResources = skosSearchEngine.findConcepts(processedQueryString, lang,
						OntIndexSearcher.EXACT_MATCH);
			} else if (searchMode.toLowerCase().contains("end")) {
				QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.ENDSWITH);
				String processedQueryString = qc.compile(term);
				retrievedResources = skosSearchEngine.findConcepts(processedQueryString, lang,
						OntIndexSearcher.EXACT_MATCH);
			} 
		}
		
		return retrievedResources;
	}
	
	
	private Collection<ARTURIResource> searchConceptWithOWLIMIndexes(String term, String searchMode, 
			String lang ){
		Collection <ARTURIResource>retrievedResources = new ArrayList<ARTURIResource>();
		try {
			String query =	"SELECT ?conceptURI"+
							"\nWHERE{";
			if(searchMode.toLowerCase().contains("exact")){
				query +="\n?label <"+LUCENEINDEX+"> \""+term+"\" .";
			} else if(searchMode.toLowerCase().contains("start")){
				query +="\n?label <"+LUCENEINDEX+"> \""+term+"*\" .";
			} else if(searchMode.toLowerCase().contains("contain")){
				query +="\n?label <"+LUCENEINDEX+"> \"*"+term+"*\" .";
			} else if(searchMode.toLowerCase().contains("end")){
				query +="\n?label <"+LUCENEINDEX+"> \"*"+term+"\" .";
			}
			if(lang.compareToIgnoreCase("all") != 0){
				query +="\nFILTER( langMatches(lang(?label), \""+lang+"\"))";
			}
			query +="\n?xlabel <"+SKOSXLLITERALFORM+"> ?label ."+
					"\n{ ?conceptURI <"+SKOSXLPREFLABEL+"> ?xlabel . } "+
					"\nUNION"+
					"\n{ ?conceptURI <"+SKOSXLALTLABEL+"> ?xlabel . }  "+
					"\nUNION"+
					"\n{ ?conceptURI <"+SKOSXLHIDDENLABEL+"> ?xlabel . }  ";
			query +="\n}";
//			logger.debug("query = "+query); // DEBUG
			
			TupleQuery tupleQuery;
		
			tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);

			TupleBindingsIterator it = tupleQuery.evaluate(true);

			while(it.hasNext()){
				TupleBindings tuple = it.getNext();
				retrievedResources.add(tuple.getBinding("conceptURI").getBoundValue().asURIResource());
			}
			it.close();
			
			
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
		return retrievedResources;
	}
	
	private Collection<ARTLiteral> searchLabelsCaseInsensitive(String term, String searchMode, String lang) 
			throws IndexAccessException, ParseException{
		Collection<ARTLiteral> retrievedResources = new ArrayList<ARTLiteral>();
		Collection<ARTLiteral> tempRetrievedResources = new ArrayList<ARTLiteral>();
		List<ARTLiteral> differentLiteralsList = new ArrayList<ARTLiteral>();
		List<String> differentLiteralsStringList = new ArrayList<String>();
		
		if(useOWLIMIndexes)
			return searchLabelsWithOWLIMIndexes(term, searchMode, lang, true);
		
		//else, do not use the OWLIM indexes, use the local indexes
		
		
		//use the term as it is;
		tempRetrievedResources = searchLabels(term, searchMode, lang);
		if(tempRetrievedResources == null)
			return null;
		for(ARTLiteral literal : tempRetrievedResources){
			if(!differentLiteralsStringList.contains(literal.getLabel()+"@"+literal.getLanguage())){
				differentLiteralsStringList.add(literal.getLabel()+"@"+literal.getLanguage());
				differentLiteralsList.add(literal);
			}
		}
		//consider the term with all the letter to lower case
		String termToLowerCase = term.toLowerCase();
		if(term.compareTo(termToLowerCase) != 0){
			tempRetrievedResources = searchLabels(termToLowerCase, searchMode, lang);
			for(ARTLiteral literal : tempRetrievedResources){
				if(!differentLiteralsStringList.contains(literal.getLabel()+"@"+literal.getLanguage())){
					differentLiteralsStringList.add(literal.getLabel()+"@"+literal.getLanguage());
					differentLiteralsList.add(literal);
				}
			}
		}
		//consider the term with all the letter to upper case
		String termToUpperCase = term.toUpperCase();
		if(term.compareTo(termToUpperCase) != 0){
			tempRetrievedResources = searchLabels(termToUpperCase, searchMode, lang);
			for(ARTLiteral literal : tempRetrievedResources){
				if(!differentLiteralsStringList.contains(literal.getLabel()+"@"+literal.getLanguage())){
					differentLiteralsStringList.add(literal.getLabel()+"@"+literal.getLanguage());
					differentLiteralsList.add(literal);
				}
			}
		}
		//consider the term with the first letter to upper case and the rest to lower case
		String termFirstLetterUpper = term.substring(0,1).toUpperCase()+term.substring(1).toLowerCase();
		if(term.compareTo(termFirstLetterUpper) !=0 ){
			tempRetrievedResources = searchLabels(termFirstLetterUpper, searchMode, lang);
			for(ARTLiteral literal : tempRetrievedResources){
				if(!differentLiteralsStringList.contains(literal.getLabel()+"@"+literal.getLanguage())){
					differentLiteralsStringList.add(literal.getLabel()+"@"+literal.getLanguage());
					differentLiteralsList.add(literal);
				}
			}
		}
		//System.out.println("term = "+term+"\n\ttermToLowerCase = "+termToLowerCase+
		//		"\n\ttermToUpperCase="+termToUpperCase+"\n\ttermFirstLetterUpper="+termFirstLetterUpper); // DEBUG
		
		for(ARTLiteral literal : differentLiteralsList)
			retrievedResources.add(literal);
		
		
		return retrievedResources;
	}
	
	private Collection<ARTLiteral> searchLabels(String term, String searchMode, String lang) 
			throws IndexAccessException, ParseException{
		
		if(useOWLIMIndexes)
			return searchLabelsWithOWLIMIndexes(term, searchMode, lang, false);
		
		Collection<ARTLiteral> retrievedResources = new ArrayList<ARTLiteral>();
		if(lang.compareToIgnoreCase("all") == 0){
			//search with all the possible languages
			for(int i=0; i<languages.length; ++i) {
				Collection<String> tempRetrievedResources = null;
				// the possible searchMode are: contains, exact match, starts with, ends with
				//if (searchMode.compareToIgnoreCase("exact match") == 0) {
				if (searchMode.toLowerCase().contains("word") ) { // this is exact word
					QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.CONTAINS);
					String processedQueryString = qc.compile(term);
					tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept,
							processedQueryString, languages[i], OntIndexSearcher.EXACT_MATCH);
				} else if (searchMode.toLowerCase().contains("exact") ) {// this is exact match
					QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.EXACT);
					String processedQueryString = qc.compile(term);
					tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept,
							processedQueryString, languages[i], OntIndexSearcher.EXACT_MATCH);
				} else if (searchMode.toLowerCase().contains("contain")) {
					QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.CONTAINS);
					String processedQueryString = qc.compile(term);
					tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept,
							processedQueryString, languages[i], OntIndexSearcher.EXACT_MATCH);
				} else if (searchMode.toLowerCase().contains("start")) {
					QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.STARTSWITH);
					String processedQueryString = qc.compile(term);
					tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept,
							processedQueryString, languages[i], OntIndexSearcher.EXACT_MATCH);
				} else if (searchMode.toLowerCase().contains("end")) {
					QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.ENDSWITH);
					String processedQueryString = qc.compile(term);
					tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept,
							processedQueryString, languages[i], OntIndexSearcher.EXACT_MATCH);
				} else {
					return null;
				}
				//contruct the literals using their languages
				for(String literals : tempRetrievedResources){
					// before adding the literal returned from the search, the string must be splitted
					// and each element should be checked if it should return or not
					// this splitting now should be useless because it is done by the search API itself 
					String[] literalStringArray = literals.split(RDFIndexManager._labelSplittingSeq);
					for(String literalString : literalStringArray){
						if(literalString.trim().length() == 0)
							continue;
						if(checkLiteral(term, searchMode, literalString))
							retrievedResources.add(skosxlModel.createLiteral(literalString, 
									languages[i]));
					}
				}
				//retrievedResources.addAll(tempRetrievedResources);
			}
		} else{
			// the possible searchMode are (searching for these strings): contain, exact, start, end
			Collection<String> tempRetrievedResources = null;
			if (searchMode.toLowerCase().contains("word") ) { // this is exact word
				QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.CONTAINS);
				String processedQueryString = qc.compile(term);
				tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept, 
						processedQueryString, lang, OntIndexSearcher.EXACT_MATCH);
			} else if (searchMode.toLowerCase().contains("exact") ) { // this is exact match
				QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.EXACT);
				String processedQueryString = qc.compile(term);
				tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept, 
						processedQueryString, lang, OntIndexSearcher.EXACT_MATCH);
			} else if (searchMode.toLowerCase().contains("contain")) {
				QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.CONTAINS);
				String processedQueryString = qc.compile(term);
				tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept, 
						processedQueryString, lang, OntIndexSearcher.EXACT_MATCH);
			} else if (searchMode.toLowerCase().contains("start")) {
				QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.STARTSWITH);
				String processedQueryString = qc.compile(term);
				tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept, 
						processedQueryString, lang, OntIndexSearcher.EXACT_MATCH);
			} else if (searchMode.toLowerCase().contains("end")) {
				QueryCompiler qc = new SimpleUserQueryCompiler(QueryModifier.ENDSWITH);
				String processedQueryString = qc.compile(term);
				tempRetrievedResources = skosSearchEngine.findLabelsForType(RDFResourceRolesEnum.concept, 
						processedQueryString, lang, OntIndexSearcher.EXACT_MATCH);
			} else {
				return null;
			}
			//contruct the literals using their languages
			for(String literals : tempRetrievedResources){
				// before adding the literal returned from the search, the string must be splitted
				// and each element should be checked if it should return or not
				// this splitting now should be useless because it is done by the search API itself 
				String[] literalStringArray = literals.split(RDFIndexManager._labelSplittingSeq);
				for(String literalString : literalStringArray){
					if(literalString.trim().length() == 0)
						continue;
					if(checkLiteral(term, searchMode, literalString))
						retrievedResources.add(skosxlModel.createLiteral(literalString, lang));
				}
			}
		}
		return retrievedResources;
	}
	
	private Collection<ARTLiteral> searchLabelsWithOWLIMIndexes(String term, String searchMode, 
			String lang, boolean caseInsensitive){
		Collection <ARTLiteral> retrievedResources = new ArrayList<ARTLiteral>();
		
		try {
			String caseInsensitiveFilterParam="";
			if(caseInsensitive )
				caseInsensitiveFilterParam = " , \"i\"";
			
			String query =	"SELECT ?label"+
							"\nWHERE{";
			if(searchMode.toLowerCase().contains("exact")){
				query +="\n?label <"+LUCENEINDEX+"> \""+term+"\" ."+
						"\nFILTER regex(str(?label), \"\\\\b"+term+"\\\\b\""+
						caseInsensitiveFilterParam+")";
			} else if(searchMode.toLowerCase().contains("start")){
				query +="\n?label <"+LUCENEINDEX+"> \""+term+"*\" ."+
						"\nFILTER regex(str(?label), \"\\\\b"+term+"\""+
						caseInsensitiveFilterParam+")";
			} else if(searchMode.toLowerCase().contains("contain")){
				query +="\n?label <"+LUCENEINDEX+"> \"*"+term+"*\" ."+
						"\nFILTER regex(str(?label), \""+term+"\""+
						caseInsensitiveFilterParam+")";
			} else if(searchMode.toLowerCase().contains("end")){
				query +="\n?label <"+LUCENEINDEX+"> \"*"+term+"\" ."+
						"\nFILTER regex(str(?label), \""+term+"\\\\b\""+
						caseInsensitiveFilterParam+")";
			}
			if(lang.compareToIgnoreCase("all") != 0){
				query +="\nFILTER( langMatches(lang(?label), \""+lang+"\"))";
			}
			query +="\n?xlabel <"+SKOSXLLITERALFORM+"> ?label ."+
					"\n{ ?conceptURI <"+SKOSXLPREFLABEL+"> ?xlabel . } "+
					"\nUNION"+
					"\n{ ?conceptURI <"+SKOSXLALTLABEL+"> ?xlabel . }  "+
					"\nUNION"+
					"\n{ ?conceptURI <"+SKOSXLHIDDENLABEL+"> ?xlabel . }  ";
			query +="\n}";
//			logger.debug("query = "+query); // DEBUG
			
			TupleQuery tupleQuery;
		
			tupleQuery = (TupleQuery)skosxlModel.createTupleQuery(QueryLanguage.SPARQL, query);

			TupleBindingsIterator it = tupleQuery.evaluate(true);

			while(it.hasNext()){
				TupleBindings tuple = it.getNext();
				retrievedResources.add(tuple.getBinding("label").getBoundValue().asLiteral());
			}
			it.close();
		
			
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
		return retrievedResources;
	}
	
	private Collection<ARTURIResource> searchLabels(String term, String searchMode, 
			String lang, boolean caseInsensitive){
		Collection <ARTURIResource> retrievedResources = new ArrayList<ARTURIResource>();
		
		return retrievedResources;
	}
	
	
	private boolean checkLiteral(String term, String searchMode, String literal ){
		//return true;
		
		if (searchMode.toLowerCase().contains("exact") ) {
			Pattern p = Pattern.compile("\\b"+term+"\\b");
			Matcher m = p.matcher(literal);
			return m.find();
		} else if (searchMode.toLowerCase().contains("contain")) {
			Pattern p = Pattern.compile(".*"+term+".*");
			Matcher m = p.matcher(literal);
			return m.find();
		} else if (searchMode.toLowerCase().contains("start")) {
			Pattern p = Pattern.compile("\\b"+term);
			Matcher m = p.matcher(literal);
			return m.find();
		} else if (searchMode.toLowerCase().contains("end")) {
			Pattern p = Pattern.compile(literal+"\\b");
			Matcher m = p.matcher(literal);
			return m.find();
		} 
		return false;
	}
	
	
	private void printQuery(String query){
		if(printQueryInDebug){
			logger.debug("SPARQL query = "+query);
		}
	}
	
	private ARTStatementIterator removeDuplicateSTatement(ARTStatementIterator originalIter) 
			throws ModelAccessException{
		List <ARTStatement> statList = new ArrayList<ARTStatement>();
		List <String> statStringList = new ArrayList<String>();
	
		while(originalIter.streamOpen()){
			ARTStatement artStat = originalIter.getNext();
			String subj = artStat.getSubject().getNominalValue();
			String pred = artStat.getPredicate().getNominalValue();
			String obj;
			if(artStat.getObject().isLiteral()){
				ARTLiteral objLiteral = artStat.getObject().asLiteral();
				if(objLiteral.getLanguage()!= null)
					obj = objLiteral.getLabel()+"@"+objLiteral.getLanguage();
				else if(objLiteral.getDatatype() != null)
					obj = objLiteral.getLabel()+"^^"+objLiteral.getDatatype().getURI();
				else
					obj = objLiteral.getLabel();
			}
			else{
				obj = artStat.getObject().getNominalValue();
			}
			String key = subj+"|_|"+pred+"|_|"+obj;
			if(!statStringList.contains(key)){
				statStringList.add(key);
				statList.add(artStat);
			}
		}
		originalIter.close();
		
		return RDFIterators.createARTStatementIterator(statList.iterator());
	}

	/*******************************************************
	 * 
	 * PRIVATE CLASSES
	 * 
	 *******************************************************/
	
	// be careful that this method uses NARROWER, which is rarely used in real case scenarios
	private class ConceptInfoRDFIterator implements RDFIterator<ARTStatement> {

		RDFIterator<? extends ARTStatement> rdfIterator;
		Iterator<? extends ARTStatement> finalIterator;

		public ConceptInfoRDFIterator(ARTURIResource concept) throws ModelAccessException {
			PropertyChainsTree propChainTree = new PropertyChainsTree();
			// propChainTree.addChainedProperty(skosxlModel.createURIResource(PREFLABEL));
			// propChainTree.addChainedProperty(skosxlModel.createURIResource(HIDDENLABEL));
			// propChainTree.addChainedProperty(skosxlModel.createURIResource(ALTLABEL));
			propChainTree.addChainedProperty(skosxlModel.createURIResource(NARROWER));
			finalIterator = ModelUtilities.createCustomCBD(skosxlModel, concept, true, propChainTree,
					NodeFilters.ANY).iterator();
			// finalIterator = ModelUtilities.createCBD(skosxlModel, concept, true,
			// NodeFilters.ANY).iterator();
		}

		public void close() throws ModelAccessException {
			// rdfIterator.close();
		}

		public ARTStatement getNext() throws ModelAccessException {
			return finalIterator.next();
		}

		public boolean streamOpen() throws ModelAccessException {
			return finalIterator.hasNext();
		}

		public boolean hasNext() {
			return finalIterator.hasNext();
		}

		public ARTStatement next() {
			return finalIterator.next();
		}

		public void remove() {
			finalIterator.remove();
		}
	}

	private class PrefLabelsRDFIterator implements RDFIterator<ARTStatement> {

		RDFIterator<? extends ARTStatement> rdfIterator;
		Iterator<? extends ARTStatement> finalIterator;

		public PrefLabelsRDFIterator(ARTURIResource concept) throws ModelAccessException {
			Collection<ARTStatement> list = new ArrayList<ARTStatement>();
			ARTLiteralIterator iter = skosxlModel.listPrefLabels(concept, true, NodeFilters.ANY);
			ARTURIResource pred = skosxlModel
					.createURIResource(SKOSXLPREFLABEL);
			while (iter.hasNext()) {
				ARTLiteral literal = iter.next();
				ARTStatement artStatement = skosxlModel.createStatement(concept, pred, literal);
				list.add(artStatement);
			}

			finalIterator = (Iterator<? extends ARTStatement>) list.iterator();
		}

		public void close() throws ModelAccessException {
			// rdfIterator.close();
		}

		public ARTStatement getNext() throws ModelAccessException {
			return finalIterator.next();
		}

		public boolean streamOpen() throws ModelAccessException {
			return finalIterator.hasNext();
		}

		public boolean hasNext() {
			return finalIterator.hasNext();
		}

		public ARTStatement next() {
			return finalIterator.next();
		}

		public void remove() {
			finalIterator.remove();
		}
	}

	private class AllLabelsRDFIterator implements RDFIterator<ARTStatement> {

		RDFIterator<? extends ARTStatement> rdfIterator;
		Iterator<? extends ARTStatement> finalIterator;

		public AllLabelsRDFIterator(ARTURIResource concept) throws ModelAccessException {
			PropertyChainsTree propChainTree = new PropertyChainsTree();
			propChainTree.addChainedProperty(skosxlModel.createURIResource(PREFLABEL));
			propChainTree.addChainedProperty(skosxlModel.createURIResource(ALTLABEL));
			propChainTree.addChainedProperty(skosxlModel.createURIResource(HIDDENLABEL));
			finalIterator = ModelUtilities.createCustomCBD(skosxlModel, concept, true, propChainTree,
					NodeFilters.ANY).iterator();
		}

		public void close() throws ModelAccessException {
			// rdfIterator.close();
		}

		public ARTStatement getNext() throws ModelAccessException {
			return finalIterator.next();
		}

		public boolean streamOpen() throws ModelAccessException {
			return finalIterator.hasNext();
		}

		public boolean hasNext() {
			return finalIterator.hasNext();
		}

		public ARTStatement next() {
			return finalIterator.next();
		}

		public void remove() {
			finalIterator.remove();
		}
	}

}
