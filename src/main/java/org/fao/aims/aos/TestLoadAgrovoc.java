package org.fao.aims.aos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2DirectAccessModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2PersistentInMemoryModelConfiguration;

public class TestLoadAgrovoc {

	private static String BaseModelTest = "repDir";
	private static String rdfInputFilePath = "agrovoc.nt";

	/**
	 * @param args
	 * @throws UnloadableModelConfigurationException
	 * @throws UnsupportedModelConfigurationException
	 * @throws ModelCreationException
	 * @throws UnsupportedRDFFormatException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException, ModelCreationException, FileNotFoundException,
			IOException, ModelAccessException, ModelUpdateException, UnsupportedRDFFormatException {

		// usual creation of a Sesame2 model factory
		ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();

		// a model configuration is created for the Sesame2 implementation. Note that there are two contraints
		// here:
		// first it has been created from a Sesame2 model factory
		// the configuration is a "parameters bundle" especially suited for "persisting" "in-memory" sesame
		// repositories.
		Sesame2DirectAccessModelConfiguration modelConf = factImpl
				.createModelConfigurationObject(Sesame2PersistentInMemoryModelConfiguration.class);

		// the specific class selected for the configuration has determined much of nature of the models which
		// will be created
		// with that configuration, however the configuration maybe further customized by setting the
		// following parameters
		modelConf.directTypeInference = false;
		modelConf.rdfsInference = false;

		// a factory is created in the usual way. Note that it is possible to contrain the factory to only
		// accept configurations of a given type. This is useful for static code to lessen chances of
		// configuration misuse. The java generics constraint is however erased at runtime
		OWLArtModelFactory<Sesame2ModelConfiguration> fact = OWLArtModelFactory.createModelFactory(factImpl);

		System.out.println("prima1\n");
		SKOSModel skosModel = fact.loadSKOSModel(null, BaseModelTest, modelConf);
		System.out.println("dopo1\n");

		System.out.println("prima2\n");
		// per importare un file RDF(XML) creato ad es con Protege o Sematic Turkey
		String importedUri = null;// quella di agrovoc
		skosModel.addRDF(new File(rdfInputFilePath), importedUri, RDFFormat.NTRIPLES, NodeFilters.MAINGRAPH);
		System.out.println("dopo2\n");

	}

}
