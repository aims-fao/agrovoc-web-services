package org.fao.aims.aos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestFindLanguages {


	private String filePath = "agrovoc_wb_20110223_v_1_3.nt";
	
	public static void main(String[] args) {

		long start = System.currentTimeMillis();
		TestFindLanguages testFindLanguages = new TestFindLanguages();

		List<String> langList = testFindLanguages.getLanguages();
		
		testFindLanguages.printLanguages(langList);
		
		long end = System.currentTimeMillis();
		String prettyTime = "Program terminated after " + testFindLanguages.printPrettyTime(start, end);
		System.out.println(prettyTime);

	}
	
	private List<String> getLanguages(){
		List<String> languagesList = new ArrayList<String>();
		int cont = 0;
		try {
			File file = new File(filePath);
			BufferedReader bf = new BufferedReader(new FileReader(file));
			String line;
			String [] lineArray;
			while ((line = bf.readLine()) != null) {
				++cont;
				if(cont%1000 == 0)
					System.out.print(".");
				if(cont%100000 == 0)
					System.out.println("*");
				lineArray = line.split("@");
				if(lineArray.length == 1)
					continue;
				String language = lineArray[1].substring(0, 2);
				if(languagesList.contains(language))
					continue;
				languagesList.add(language);
			}
			bf.close();
			System.out.println("\nRead "+cont+" triples");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return languagesList;
	}

	
	public String printPrettyTime(long start, long end) {
		long secTotal = (end - start) / 1000;
		long sec = secTotal % 60;
		long minTotal = secTotal / 60;
		long min = minTotal % 60;
		long hourTotal = minTotal / 60;

		String prettyTime = "";
		if (hourTotal < 10)
			prettyTime += "0";
		prettyTime += hourTotal + ":";
		if (min < 10)
			prettyTime += "0";
		prettyTime += min + ":";
		if (sec < 10)
			prettyTime += "0";
		prettyTime += sec;

		return prettyTime;
	}
	
	private void printLanguages(List<String> langList) {
		System.out.println("language:");
		for(String lang : langList){
			System.out.println("\t"+lang);
		}
		
	}
}
