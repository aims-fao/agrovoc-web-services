package org.fao.aims.aos;

public class WSDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("starting the program");
		WSDemo wsDemo = new WSDemo();
		
		long start = System.currentTimeMillis();
		
		SKOSWS skosWS = new SKOSWS();
		
		
		// Test getConceptByKeyword
		//wsDemo.testGetConceptByKeyword(skosWS);
		
		// Test getConceptByKeyword2
		//wsDemo.testGetConceptByKeyword2(skosWS);
				
		//Test searchByModeLangScopeXML
		//wsDemo.testSearchByModeLangScopeXML(skosWS);
		
		//Test simpleSearchByMode2
		wsDemo.testSimpleSearchByMode2(skosWS);
		
		//Test getConceptInfoByTermcode
		//wsDemo.testGetConceptInfoByTermcode(skosWS);
		
		//Test getConceptInfoByURI
		//wsDemo.testGetConceptInfoByURI(skosWS);
		
		//Test getDefinitions
		//wsDemo.testGetDefinitions(skosWS);
		
		//Test getAllLabelsByTermcode2
		//wsDemo.testGetAllLabelsByTermcode2(skosWS);
		
		//Test getTermByLanguage
		//wsDemo.testGetTermByLanguage(skosWS);
		
		//Test getURIByTermAndLangXML
		//wsDemo.testGetURIByTermAndLangXML(skosWS);
		
		//Test getFullAuthority
		//wsDemo.testGetFullAuthority(skosWS);
		
		//Test getConceptByURI
		//wsDemo.testGetConceptByURI(skosWS);
		
		//Test getConceptByRelationshipValue
		//wsDemo.testGetConceptByRelationshipValue(skosWS);
		
		//Test getlatestUpdates
		//wsDemo.testGetlatestUpdates(skosWS);
		
		//Test getRelationByTermcodeXML
		//wsDemo.testGetRelationByTermcodeXML(skosWS);
		
		//Test getTermcodeByTermAndLangXML
		//wsDemo.testGetTermcodeByTermAndLangXML(skosWS);
		
		//Other tests
		//wsDemo.otherTests(skosWS);
		
		//skosWS.close();
		
		long end = System.currentTimeMillis();
		String prettyTime = "Program terminated after " + wsDemo.printPrettyTime(start, end);
		System.out.println(prettyTime);
	}
	
	public void testGetConceptByKeyword(SKOSWS skosWS){
		System.out.println("starting testGetConceptByKeyword");
		long start = System.currentTimeMillis();
		
		String ontologyName="Agrovoc";
		String format;
		String lang;
		String searchMode;
		String term;
		
		String result="";
		format = "SKOS";
		
		lang = "en";
		searchMode = "starts with";
		term = "wine";
		lang = "it";
		//result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang);
		//System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
		//		+searchMode+", "+ format+", "+ lang+") =\n"+result);
		
		
		lang = "it";
		searchMode = "starts with";
		term = "port";
		//result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang);
		//System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
		//		+searchMode+", "+ format+", "+ lang+") =\n"+result);
		
		lang = "it";
		searchMode = "ends with";
		term = "orto";
		//result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang);
		//System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
		//		+searchMode+", "+ format+", "+ lang+") =\n"+result);
		

		lang = "it";
		searchMode = "contains";
		term = "ort";
		//result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang);
		//System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
		//		+searchMode+", "+ format+", "+ lang+") =\n"+result);
		

		lang = "it";
		searchMode = "contains";
		term = "orto";
		result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang);
		System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
				+searchMode+", "+ format+", "+ lang+") =\n"+result);
		

		lang = "it";
		searchMode = "contains";
		term = "orto";
		format = "txt";
		result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang);
		System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
				+searchMode+", "+ format+", "+ lang+") =\n"+result);
		
		lang = "it";
		searchMode = "contains";
		term = "orto";
		format = "URI-txt";
		//result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang);
		//System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
		//		+searchMode+", "+ format+", "+ lang+") =\n"+result);

		lang = "it";
		searchMode = "exact match";
		term = "vino porto";
		format = "skos";
		//result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang);
		//System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
		//		+searchMode+", "+ format+", "+ lang+") =\n"+result);
		
		lang = "en";
		searchMode = "Exact Match";
		term = "Rice";
		format = "SKOS";
		//result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang); 
		//System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
		//		+searchMode+", "+ format+", "+ lang+") =\n"+result);
		
		lang = "en";
		searchMode = "contains";
		term = "food";
		format = "skos";
		result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang); 
		System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
				+searchMode+", "+ format+", "+ lang+") =\n"+result);
		
		
		lang = "all";
		searchMode = "contains";
		term = "metody";
		format = "skos";
		result = skosWS.getConceptByKeyword(ontologyName, term, format, searchMode, lang); 
		System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
				+searchMode+", "+ format+", "+ lang+") =\n"+result);
		
		printTimeForTest(start, "getConceptByKeyword");
	}
	
	public void testGetConceptByKeyword2(SKOSWS skosWS){
		System.out.println("starting testGetConceptByKeyword2");
		long start = System.currentTimeMillis();
		
		String ontologyName="Agrovoc";
		String format;
		String lang;
		String searchMode;
		String term;
		String outlang;
		
		String result="";
		format = "SKOS";
		
		
		lang = "en";
		searchMode = "contains";
		term = "food";
		format = "skos";
		outlang = "en";
		result = skosWS.getConceptByKeyword2(ontologyName, term, format, searchMode, lang, outlang); 
		System.out.println("\ngetConceptByKeyword("+ontologyName+", "+ term+", "
				+searchMode+", "+ format+", "+ lang+", "+outlang+") =\n"+result);
		
		
		printTimeForTest(start, "getConceptByKeyword2");
	}
	
	public void testSearchByModeLangScopeXML(SKOSWS skosWS){
		System.out.println("starting testSearchByModeLangScopeXML");
		long start = System.currentTimeMillis();
		
		String lang="", searchMode="", term="", result="",ontologyName="", format="",
				scopeID="";
		ontologyName = "agrovoc";
		String [] outputLang;
		
		/*lang = "all";
		outputLang= new String [2]; 
		outputLang[0] = "it"; 
		outputLang[1] = "en"; 
		searchMode = "contains";
		term = "orto";
		format = "skos";
		scopeID = "All terms";
		result = skosWS.searchByModeLangScopeXML(ontologyName, term, format, searchMode, lang, outputLang,
				scopeID);
		System.out.println("\nsearchByModeLangScopeXML("+ontologyName+", "+ term+", "
				+searchMode+", "+lang+", "+ArrayToString(outputLang, false)+", "+scopeID+", "+format+") =\n"+result);
		
		
		lang = "it";
		outputLang= new String [2]; 
		outputLang[0] = "it"; 
		outputLang[1] = "en"; 
		searchMode = "contains";
		term = "orto";
		format = "skos";
		scopeID = "Only geographical terms";
		result = skosWS.searchByModeLangScopeXML(ontologyName, term, format, searchMode, lang, outputLang,
				scopeID);
		System.out.println("\nsearchByModeLangScopeXML("+ontologyName+", "+ term+", "
				+searchMode+", "+lang+", "+ArrayToString(outputLang, false)+", "+scopeID+", "+format+") =\n"+result);
		
		lang = "it";
		outputLang= new String[1];
		outputLang[0] = "all";
		searchMode = "contains";
		term = "orto";
		format = "skos";
		scopeID = "Only Taxonomic terms";
		result = skosWS.searchByModeLangScopeXML(ontologyName, term, format, searchMode, lang, outputLang,
				scopeID);
		System.out.println("\nsearchByModeLangScopeXML("+ontologyName+", "+ term+", "
				+searchMode+", "+lang+", "+ArrayToString(outputLang, false)+", "+scopeID+", "+format+") =\n"+result);
		

		lang = "it";
		outputLang = new String[2];
		outputLang[0]="FAO official languages";
		outputLang[1]="it";
		searchMode = "contains";
		term = "orto";
		format = "skos";
		scopeID = "All except greographical terms";
		result = skosWS.searchByModeLangScopeXML(ontologyName, term, format, searchMode, lang, outputLang,
				scopeID);
		System.out.println("\nsearchByModeLangScopeXML("+ontologyName+", "+ term+", "
				+searchMode+", "+lang+", "+ArrayToString(outputLang, false)+", "+scopeID+", "+format+") =\n"+result);
		*/
		lang = "it";
		outputLang = new String[1];
		outputLang[0]="it";
		searchMode = "contains";
		term = "orto";
		format = "txt";
		scopeID = "All terms";
		result = skosWS.searchByModeLangScopeXML(ontologyName, term, format, searchMode, lang, outputLang,
				scopeID);
		System.out.println("\nsearchByModeLangScopeXML("+ontologyName+", "+ term+", "
				+searchMode+", "+lang+", "+ArrayToString(outputLang, false)+", "+scopeID+", "+format+") =\n"+result);
		
		lang = "all";
		outputLang = new String[1];
		outputLang[0]="all";
		searchMode = "contains";
		term = "orto";
		format = "txt";
		scopeID = "All terms";
		result = skosWS.searchByModeLangScopeXML(ontologyName, term, format, searchMode, lang, outputLang,
				scopeID);
		System.out.println("\nsearchByModeLangScopeXML("+ontologyName+", "+ term+", "
				+searchMode+", "+lang+", "+ArrayToString(outputLang, false)+", "+scopeID+", "+format+") =\n"+result);
		
		
		lang = "it";
		outputLang = new String[1];
		outputLang[0]="all";
		searchMode = "contains";
		term = "orto";
		format = "txt";
		scopeID = "All terms";
		result = skosWS.searchByModeLangScopeXML(ontologyName, term, format, searchMode, lang, outputLang,
				scopeID);
		System.out.println("\nsearchByModeLangScopeXML("+ontologyName+", "+ term+", "
				+searchMode+", "+lang+", "+ArrayToString(outputLang, false)+", "+scopeID+", "+format+") =\n"+result);
		

		lang = "it";
		outputLang = new String[1];
		outputLang[0]="FAO official languages";
		searchMode = "contains";
		term = "orto";
		format = "txt";
		scopeID = "All terms";
		result = skosWS.searchByModeLangScopeXML(ontologyName, term, format, searchMode, lang, outputLang,
				scopeID);
		System.out.println("\nsearchByModeLangScopeXML("+ontologyName+", "+ term+", "
				+searchMode+", "+lang+", "+ArrayToString(outputLang, false)+", "+scopeID+", "+format+") =\n"+result);
		
		printTimeForTest(start, "searchByModeLangScopeXML");
	}
	
	public void testSimpleSearchByMode2(SKOSWS skosWS){
		System.out.println("starting testSimpleSearchByMode2");
		long start = System.currentTimeMillis();
		
		String searchMode="", term="", result="",separator="";
		
		searchMode = "startsWith";
		term = "fruit";
		separator = "---";
		result = skosWS.simpleSearchByMode2(term, searchMode, separator);
		System.out.println("\nsimpleSearchByMode2("+term+", "+searchMode+", "+separator+") =\n"+result);
		
		searchMode = "starting";
		term = "Fruit";
		separator = "---";
		result = skosWS.simpleSearchByMode2(term, searchMode, separator);
		System.out.println("\nsimpleSearchByMode2("+term+", "+searchMode+", "+separator+") =\n"+result);
		
		searchMode = "exact";
		term = "Rice";
		separator = "---";
		result = skosWS.simpleSearchByMode2(term, searchMode, separator);
		System.out.println("\nsimpleSearchByMode2("+term+", "+searchMode+", "+separator+") =\n"+result);
		
		
		
		searchMode = "contain";
		term = "metody";
		separator = "---";
		result = skosWS.simpleSearchByMode2(term, searchMode, separator);
		System.out.println("\nsimpleSearchByMode2("+term+", "+searchMode+", "+separator+") =\n"+result);
		
		searchMode = "contain";
		term = "metody";
		separator = "-";
		result = skosWS.simpleSearchByMode2(term, searchMode, separator);
		System.out.println("\nsimpleSearchByMode2("+term+", "+searchMode+", "+separator+") =\n"+result);
		
		
		printTimeForTest(start, "simpleSearchByMode2");
	}
	
	public void testGetConceptInfoByTermcode(SKOSWS skosWS){
		System.out.println("starting testGetConceptInfoByTermcode");
		long start = System.currentTimeMillis();
		
		String []result;
		String termCode;
		
		termCode = "29148";
		result = skosWS.getConceptInfoByTermcode(termCode);
		System.out.println("\ngetConceptInfoByTermcode("+termCode+") =\n"+ArrayToString(result, false));
		
		termCode = "29149";
		result = skosWS.getConceptInfoByTermcode(termCode);
		System.out.println("\ngetConceptInfoByTermcode("+termCode+") =\n"+ArrayToString(result, false));
		
		termCode = "6599";
		result = skosWS.getConceptInfoByTermcode(termCode);
		System.out.println("\ngetConceptInfoByTermcode("+termCode+") =\n"+ArrayToString(result, false));
		
		
		printTimeForTest(start, "getConceptInfoByTermcode");
	}

	public void testGetConceptInfoByURI(SKOSWS skosWS){
		System.out.println("starting testGetConceptInfoByURI");
		long start = System.currentTimeMillis();
		
		String conceptURI, format, ontologyName;
				
		String result;
		
		conceptURI = "http://aims.fao.org/aos/agrovoc/c_13522";
		format = "skos";
		ontologyName = "Agrovoc";
		result = skosWS.getConceptInfoByURI(ontologyName, conceptURI, format);
		System.out.println("\ngetConceptInfoByURI("+ontologyName+", "+ conceptURI+", "
				+ format+") =\n"+result);
		
		format = "txt";
		result = skosWS.getConceptInfoByURI(ontologyName, conceptURI, format);
		System.out.println("\ngetConceptInfoByURI("+ontologyName+", "+ conceptURI+", "
				+ format+") =\n"+result);
		
		printTimeForTest(start, "getConceptInfoByURI");		
	}

	
	public void testGetDefinitions(SKOSWS skosWS){
		System.out.println("starting testGetDefinitions");
		long start = System.currentTimeMillis();
		
		String []result;
		int termCode;
		String lang;
		
		termCode = 99;
		lang="en";
		result = skosWS.getDefinitions(termCode, lang);
		System.out.println("\ngetDefinitions("+termCode+", "+lang+") =\n"+ArrayToString(result, false));
		
		lang="it";
		result = skosWS.getDefinitions(termCode, lang);
		System.out.println("\ngetDefinitions("+termCode+", "+lang+") =\n"+ArrayToString(result, false));
		
		printTimeForTest(start, "getDefinitions");
	}

	public void testGetAllLabelsByTermcode2(SKOSWS skosWS){
		System.out.println("starting testGetAllLabelsByTermcode2");
		long start = System.currentTimeMillis();
		
		String result;
		int termCode;
		String separator;
		
		termCode = 13522;
		separator = "--";
		result = skosWS.getAllLabelsByTermcode2(termCode, separator);
		System.out.println("\ntestGetAllLabelsByTermcode2("+termCode+") =\n"+result);
		
		termCode = 29148;
		separator = "<>";
		result = skosWS.getAllLabelsByTermcode2(termCode, separator);
		System.out.println("\ngetAllLabelsByTermcode2("+termCode+") =\n"+result);
		
		printTimeForTest(start, "getAllLabelsByTermcode2");
	}

	public void testGetTermByLanguage(SKOSWS skosWS){
		System.out.println("starting testGetTermByLanguage");
		long start = System.currentTimeMillis();
		
		String result;
		int termCode;
		String lang;
		
		termCode = 13522;
		lang = "en";
		result = skosWS.getTermByLanguage(termCode, lang);
		System.out.println("\ngetTermByLanguage("+termCode+","+lang+") =\n"+result);
		
		termCode = 29148;
		lang = "en,it,fr";
		result = skosWS.getTermByLanguage(termCode, lang);
		System.out.println("\ngetTermByLanguage("+termCode+","+lang+") =\n"+result);
		
		printTimeForTest(start, "getTermByLanguage");
	}

	public void testGetURIByTermAndLangXML(SKOSWS skosWS){
		System.out.println("starting testGetURIByTermAndLangXML");
		long start = System.currentTimeMillis();
		
		String ontologyName = "Agrovoc";
		String lang, searchMode, term, format;
		String result="";
		
		lang = "it";
		searchMode = "contains";
		term = "orto";
		format = "skos";
		result = skosWS.getURIByTermAndLangXML(ontologyName, term, searchMode, format, lang);
		System.out.println("\ngetURIByTermAndLangXML("+ontologyName+","+term+","+ searchMode+","+
				format+","+ lang+") =\n"+result);
		
		lang = "it";
		searchMode = "contains";
		term = "orto";
		format = "txt";
		result = skosWS.getURIByTermAndLangXML(ontologyName, term, searchMode, format, lang);
		System.out.println("\ngetURIByTermAndLangXML("+ontologyName+","+term+","+ searchMode+","+
				format+","+ lang+") =\n"+result);
		
		printTimeForTest(start, "getURIByTermAndLangXML");
	}
	
	public void testGetFullAuthority(SKOSWS skosWS){
		System.out.println("starting testGetFullAuthority");
		long start = System.currentTimeMillis();
		
		
		printTimeForTest(start, "getFullAuthority");
	}
	
	public void testGetConceptByURI(SKOSWS skosWS){
		System.out.println("starting testGetConceptByURI");
		long start = System.currentTimeMillis();
		
		String conceptURI = "http://aims.fao.org/aos/agrovoc/c_13522";
		String format = "skos";
		String ontologyName = "Agrovoc";
				
		String result;
		
		result = skosWS.getConceptByURI(ontologyName, conceptURI, format);
		System.out.println("\ngetConceptByURI("+ontologyName+", "+ conceptURI+", "
				+ format+") =\n"+result);
		
		format = "txt";
		result = skosWS.getConceptByURI(ontologyName, conceptURI, format);
		System.out.println("\ngetConceptByURI("+ontologyName+", "+ conceptURI+", "
				+ format+") =\n"+result);
		
		
		printTimeForTest(start, "getConceptByURI");
	}
	
	public void testGetConceptByRelationshipValue(SKOSWS skosWS){
		System.out.println("starting testGetConceptByRelationshipValue");
		long start = System.currentTimeMillis();
		
		String result;
		
		String ontologyName, relationURI, format, value, searchMode, lang;
		
		ontologyName="Agrovoc";
		relationURI = "http://aims.fao.org/aos/agrontology#isProducedBy";
		format = "SKOS";
		value="http://aims.fao.org/aos/agrovoc/c_633"; 
		searchMode="startsWith";
		lang = "en";
		result = skosWS.getConceptByRelationshipValue(ontologyName, relationURI,  value, searchMode, 
				lang, format);
		System.out.println("\ngetConceptByRelationshipValue("+ontologyName+", "+ relationURI+", "+value+", "
				+searchMode+", "+lang+", "+ format+") =\n"+result);

		
		ontologyName="Agrovoc";
		relationURI = "http://aims.fao.org/aos/agrontology#isProducedBy";
		format = "SKOS";
		value="http://aims.fao.org/aos/agrovoc/c_63"; 
		searchMode="startsWith";
		lang = "en";
		result = skosWS.getConceptByRelationshipValue(ontologyName, relationURI,  value, searchMode, 
				lang, format);
		System.out.println("\ngetConceptByRelationshipValue("+ontologyName+", "+ relationURI+", "+value+", "
				+searchMode+", "+lang+", "+ format+") =\n"+result);

		
		ontologyName="Agrovoc";
		relationURI = "http://aims.fao.org/aos/agrontology#isProducedBy";
		format = "SKOS";
		value="http://aims.fao.org/aos/agrovoc/c_63"; 
		searchMode="endsWith";
		lang = "en";
		result = skosWS.getConceptByRelationshipValue(ontologyName, relationURI,  value, searchMode, 
				lang, format);
		System.out.println("\ngetConceptByRelationshipValue("+ontologyName+", "+ relationURI+", "+value+", "
				+searchMode+", "+lang+", "+ format+") =\n"+result);
		
		ontologyName="Agrovoc";
		relationURI = "http://aims.fao.org/aos/agrontology#isProducedBy";
		format = "TXT";
		value="http://aims.fao.org/aos/agrovoc/c_633"; 
		searchMode="startsWith";
		lang = "en";
		result = skosWS.getConceptByRelationshipValue(ontologyName, relationURI,  value, searchMode, 
				lang, format);
		System.out.println("\ngetConceptByRelationshipValue("+ontologyName+", "+ relationURI+", "+value+", "
				+searchMode+", "+lang+", "+ format+") =\n"+result);

		
		ontologyName="Agrovoc";
		relationURI = "http://aims.fao.org/aos/agrontology#isProducedBy";
		format = "TXT";
		value="http://aims.fao.org/aos/agrovoc/c_63"; 
		searchMode="startsWith";
		lang = "en";
		result = skosWS.getConceptByRelationshipValue(ontologyName, relationURI,  value, searchMode, 
				lang, format);
		System.out.println("\ngetConceptByRelationshipValue("+ontologyName+", "+ relationURI+", "+value+", "
				+searchMode+", "+lang+", "+ format+") =\n"+result);

		
		ontologyName="Agrovoc";
		relationURI = "http://aims.fao.org/aos/agrontology#isProducedBy";
		format = "TXT";
		value="c_633"; 
		searchMode="endsWith";
		lang = "en";
		result = skosWS.getConceptByRelationshipValue(ontologyName, relationURI,  value, searchMode, 
				lang, format);
		System.out.println("\ngetConceptByRelationshipValue("+ontologyName+", "+ relationURI+", "+value+", "
				+searchMode+", "+lang+", "+ format+") =\n"+result);

		
		printTimeForTest(start, "getConceptByRelationshipValue");
	}
	
	public void testGetlatestUpdates(SKOSWS skosWS){
		System.out.println("starting testGetlatestUpdates");
		long start = System.currentTimeMillis();
		
		String ontologyName="Agrovoc";
		String format = "SKOS";
		String dateFrom="", dateTo="";
		String result;
		result = skosWS.getLatestUpdates(ontologyName, dateFrom, dateTo, format);
		System.out.println("\ngetLatestUpdates("+ontologyName+", "+ dateFrom+", "
				+dateTo+", "+ format+") =\n"+result);

		format = "SKOS";
		dateFrom="01/01/2009";
		dateTo="";
		result = skosWS.getLatestUpdates(ontologyName, dateFrom, dateTo, format);
		System.out.println("\ngetLatestUpdates("+ontologyName+", "+ dateFrom+", "
				+dateTo+", "+ format+") =\n"+result);

		
		format = "SKOS";
		dateFrom="01/01/2009";
		dateTo="30/01/2009";
		//result = skosWS.getLatestUpdates(ontologyName, dateFrom, dateTo, format);
		//System.out.println("\ngetLatestUpdates("+ontologyName+", "+ dateFrom+", "
		//		+dateTo+", "+ format+") =\n"+result);


		format = "txt";
		//dateFrom="01/01/2009";
		//dateTo="31/01/2009";
		result = skosWS.getLatestUpdates(ontologyName, dateFrom, dateTo, format);
		System.out.println("\ngetLatestUpdates("+ontologyName+", "+ dateFrom+", "
				+dateTo+", "+ format+") =\n"+result);

		
		printTimeForTest(start, "getLatestUpdates");
	}

	
	public void testGetRelationByTermcodeXML(SKOSWS skosWS){
		System.out.println("starting testGetRelationByTermcodeXML");
		long start = System.currentTimeMillis();
		
		
		printTimeForTest(start, "getRelationByTermcodeXML");
	}
	
	public void testGetTermcodeByTermAndLangXML(SKOSWS skosWS){
		System.out.println("starting testGetTermcodeByTermAndLangXML");
		long start = System.currentTimeMillis();
		
		String ontologyName, term, lang, codeName, format;
		String result;
		
		ontologyName = "agrovoc";
		term="Rice";
		lang = "en";
		codeName = "hasCodeAgrovoc";
		format = "skos";
		result = skosWS.getTermCodeByTermAndLangXML(ontologyName, term, lang, codeName, format);
		System.out.println("\ngetTermCodeByTermAndLangXML("+ontologyName+", "+ term+", "
				+lang+", "+codeName+", "+ format+") =\n"+result);
		
		ontologyName = "agrovoc";
		term="Rice";
		lang = "en";
		codeName = "hasCodeAgrovoc";
		format = "txt";
		result = skosWS.getTermCodeByTermAndLangXML(ontologyName, term, lang, codeName, format);
		System.out.println("\ngetTermCodeByTermAndLangXML("+ontologyName+", "+ term+", "
				+lang+", "+codeName+", "+ format+") =\n"+result);
		
		printTimeForTest(start, "getTermCodeByTermAndLangXML");
	}

	
	public void otherTests(SKOSWS skosWS){
		System.out.println("starting testGetTermcodeByTermAndLangXML");
		long start = System.currentTimeMillis();
		
		String ontologyName, term, lang, codeName, format, conceptName, conceptURI, relationURI;
		String result;

		ontologyName="agrovoc";
		conceptName = "agrovoc:c_6211";
		format = "SKOS";
		result = skosWS.getConceptInfoByClassName(ontologyName, conceptName, format);
		System.out.println("\ngetConceptInfoByClassName("+ontologyName+", "+ conceptName+", "
				+format+") =\n"+result);
		
		ontologyName="agrovoc";
		conceptName = "c_6211";
		format = "txt";
		result = skosWS.getConceptInfoByClassName(ontologyName, conceptName, format);
		System.out.println("\ngetConceptInfoByClassName("+ontologyName+", "+ conceptName+", "
				+format+") =\n"+result);
		
		conceptURI = "http://aims.fao.org/aos/agrovoc/c_99";
		relationURI = "http://aims.fao.org/aos/agrontology#hasAntonym";
		result = skosWS.getRelatedValuesFromConceptURIRelationURI(ontologyName, conceptURI,
				relationURI);
		System.out.println("\ngetRelatedValuesFromConceptURIRelationURI("+ontologyName+", "+ conceptURI+", "
				+relationURI+") =\n"+result);
		
		printTimeForTest(start, "otherTests");
	}

	/*************************************************
			Private methods
	*************************************************/
	

	private String ArrayToString(String [] stringArray, boolean addParenthesis){
		String arrayToString = "";
		arrayToString = "[";
		for(int i=0; i<stringArray.length; ++i){
			if(addParenthesis)
				arrayToString += "[";
				arrayToString += stringArray[i]+",";
			if(addParenthesis)
					arrayToString += "]";
		}
		arrayToString = arrayToString.substring(0, arrayToString.length()-1)+"]";
		
		return arrayToString;
	}


	
	private void printTimeForTest(long start, String testName){
		long end = System.currentTimeMillis();
		System.out.println("start = "+start+" end = "+end+"for "+testName+" Diff = "+(end-start)+" millsec.");
		System.out.println("All the tests regarding "+testName+" lasted "+printPrettyTime(start, end));
	}
	
	private String printPrettyTime(long start, long end) {
		long secTotal = (end - start) / 1000;
		long sec = secTotal % 60;
		long minTotal = secTotal / 60;
		long min = minTotal % 60;
		long hourTotal = minTotal / 60;

		String prettyTime = "";
		if (hourTotal < 10)
			prettyTime += "0";
		prettyTime += hourTotal + ":";
		if (min < 10)
			prettyTime += "0";
		prettyTime += min + ":";
		if (sec < 10)
			prettyTime += "0";
		prettyTime += sec;

		return prettyTime;
	}

}
