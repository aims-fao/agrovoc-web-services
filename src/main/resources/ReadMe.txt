At the moment the only implemented services/methods are:
 - getConceptByKeyword
 - searchByModeLangScopeXML
 - simpleSearchByMode2
 - getConceptInfoByTermcode
 - getConceptInfoByURI
 - getDefinitions
 - getAllLabelsByTermcode2
 - getTermByLanguage
 - getURIByTermAndLangXML
 - getFullAuthority
 - getConceptByURI
 - getConceptByRelationshipValue
 - getlatestUpdates
 - getTermcodeByTermAndLangXML
 - getTermExpansion
 - getWebServicesVersion (new service)
 - getReleaseDate (new service)
 
 
 
The list of the required services/method is:
 - getConceptByKeyword
 - searchByModeLangScopeXML
 - simpleSearchByMode2
 - getConceptInfoByTermcode
 - getConceptInfoByURI
 - getDefinitions
 - getAllLabelsByTermcode2
 - getTermByLanguage
 - getURIByTermAndLangXML
 - getFullAuthority
 - getConceptByURI
 - getConceptByRelationshipValue
 - getlatestUpdates
 - getTermcodeByTermAndLangXML
 - getTermExpansion
 
 
 
The list services/methods which will be implemented is:
 
 
 
 /******************************************************/
OTHER USEFUL INFORMATION

To do a quick test of these methods use the WSDemo class and uncomment the desired methods/services
 
 In the configWS.properties there are the path/files needed by the application/web service
 
 The first time it is executed it can take a long time because it has to read and parse the ontology to build the model 
 (which will be used the next times) and expecially because it has to build the indexes used by the searches 
 
 To force a reload of the ontology/thesaurus and the relative indexes set to true the parameter forceReload in 
 the configWS.properties. At the next execution of the WS the ontology/thesaurus and the indexes are recreated
 
 It is possible to connect to a remote repository which uses Sesame2. At the moment this as been tested with 
 OWLIM-SE. To connect to a remote repostory specify REMOTE in the property file regarding the property 
 useREMOTEorLOCAL. In both cases (REMOTE or LOCAL)a Sesame2 repository will be used. Remember to specify in the 
 all the required parameters (password, username, url and repositoryId)
